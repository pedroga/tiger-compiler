%{
#include <cstdio>
#include <cstdlib>
#include <cstdarg>
#include <unistd.h>

#include "ast.hh"

extern FILE *yyin;
extern FILE *outfile;
extern int yylex();
extern void yyerror(char *s, ...);

Ast *ast;

%}
%locations

%union {
    // Lists.
    struct AstDecList *decs;
    struct AstExprList *exprs;
    struct AstContractList *contracts;

    // Declarations.
    struct AstDecNode *dec;
    struct AstTyNode *ty;
    struct AstTyRecordFieldNode *tyfield;

    // Expressions.
    struct AstExprNode *expr;
    struct AstAssignNode *assign;
    struct AstFuncallNode *funcall;
    struct AstRecordCreationNode *recordcreation;
    struct AstRecordArgsList *recordargs;
    struct AstArrayCreationNode *arraycreation;
    struct AstIfNode *conditional;
    struct AstWhileNode *whilee;
    struct AstForNode *forr;

    // Lvalues.
    struct AstLvalNode *lval;

    // Primitive types.
    long num;
    char *str;
}

%precedence TT_ID
%precedence TT_THEN TT_DO
%precedence TT_ASSIGN TT_OF TT_LBRACKET TT_ELSE
%left TT_OR
%left TT_AND
%nonassoc TT_EQ TT_NEQ TT_LE TT_LT TT_GT TT_GE
%left TT_PLUS TT_MINUS
%left TT_MUL TT_DIV
%precedence UMINUS


%token                  TT_TYPE TT_VAR TT_FUNC TT_ARRAY TT_OF TT_NIL
%token                  TT_IF TT_THEN TT_ELSE
%token                  TT_WHILE TT_FOR TT_TO TT_DO TT_BREAK
%token                  TT_LET TT_IN TT_OUT TT_END
%token  <num>           TT_INTEGER
%token  <str>           TT_STRING TT_ID
%token                  TT_LPARENS TT_RPARENS TT_LBRACKET TT_RBRACKET TT_LBRACE TT_RBRACE
%token                  TT_PLUS TT_MINUS TT_MUL TT_DIV
%token                  TT_EQ TT_NEQ TT_LT TT_LE TT_GT TT_GE
%token                  TT_AND TT_OR
%token                  TT_COMMA TT_COLON TT_SEMICOLON TT_DOT
%token                  TT_ASSIGN
%token                  TT_EOF


%type   <root>          root
%type   <decs>          decs
%type   <exprs>         exprs
%type   <dec>           dec
%type   <dec>           tydec
%type   <ty>            ty
%type   <tyfield>       tyfields
%type   <dec>           vardec
%type   <dec>           fundec
%type   <contracts>     contracts
%type   <expr>          expr
%type   <expr>          let_expr
%type   <expr>          int_expr
%type   <assign>        assign_expr
%type   <funcall>       funcall_expr
%type   <exprs>         funcall_args
%type   <expr>          exprseq_expr
%type   <lval>          lval
%type   <recordcreation> record_creation
%type   <recordargs>    record_args
%type   <arraycreation> array_creation
%type   <conditional>   cond_expr
%type   <whilee>        while_expr
%type   <forr>          for_expr

%start root

%%

root: TT_EOF {
        puts("Empty program, not doing anything.");
        exit(0);
    }
    | expr TT_EOF {
        ast = new Ast($1);
        return true;
    }
    ;

exprs:
    expr {
        $$ = new AstExprList(@1, $1, nullptr);
    }
    | expr TT_SEMICOLON exprs {
        $$ = new AstExprList(@1, $1, $3);
    }
    ;

expr:
    /* No value expr. */
    TT_LPARENS TT_RPARENS {
        $$ = new AstNovalNode(@1);
    }
    | TT_STRING {
        $$ = new AstStringNode(@1, $1);
    }
    | TT_INTEGER {
        $$ = new AstIntegerNode(@1, $1);
    }
    | TT_NIL {
        $$ = new AstNilNode(@1);
    }
    | TT_BREAK {
        $$ = new AstBreakNode(@1);
    }
    | exprseq_expr
    | let_expr
    | int_expr /* Arithmetic/Logic operations. */
    | assign_expr {
        $$ = $1;
    }
    | funcall_expr {
        $$ = $1;
    }
    | lval {
        $$ = new AstLvalExprNode(@1, $1);
    }
    | record_creation {
        $$ = $1;
    }
    | array_creation {
        $$ = $1;
    }
    | cond_expr {
        $$ = $1;
    }
    | while_expr {
        $$ = $1;
    }
    | for_expr {
        $$ = $1;
    }
    ;

decs:
    dec {
        $$ = new AstDecList(@1, $1, nullptr);
    }
    | dec decs {
        $$ = new AstDecList(@1, $1, $2);
    }
    ;

dec:
    tydec
    | vardec
    | fundec
    ;

tydec:
    TT_TYPE TT_ID TT_EQ ty {
        $$ = new AstTydecNode(@2, $2, $4);
    }
    ;

ty:
    TT_ID {
        $$ = new AstTyAliasNode(@1, $1);
    }
    // Empty braces.
    | TT_LBRACE TT_RBRACE {
        $$ = new AstTyRecordNode(@1, nullptr);
    }
    | TT_LBRACE tyfields TT_RBRACE {
        $$ = new AstTyRecordNode(@1, $2);
    }
    | TT_ARRAY TT_OF TT_ID {
        $$ = new AstTyArrayNode(@1, $3);
    }
    ;

tyfields:
    TT_ID TT_COLON TT_ID {
        $$ = new AstTyRecordFieldNode(@1, $1, $3, nullptr);
    }
    | TT_ID TT_COLON TT_ID TT_COMMA tyfields {
        $$ = new AstTyRecordFieldNode(@1, $1, $3, $5);
    }
    ;

vardec:
    TT_VAR TT_ID TT_COLON TT_ID TT_ASSIGN expr {
        $$ = new AstVardecNode(@1, $2, $4, $6);
    }
    | TT_VAR TT_ID TT_ASSIGN expr {
        $$ = new AstVardecNode(@1, $2, nullptr, $4);
    }
    ;

contracts:
    %empty {
        $$ = nullptr;
    }
    | TT_IN TT_LPARENS expr TT_RPARENS contracts {
        $$ = new AstContractList(@1, Contract::in, $3, $5, nullptr);
    }
    | TT_OUT TT_LPARENS TT_SEMICOLON expr TT_RPARENS contracts {
        $$ = new AstContractList(@1, Contract::out, $4, $6, nullptr);
    }
    | TT_OUT TT_LPARENS TT_ID TT_SEMICOLON expr TT_RPARENS contracts {
        $$ = new AstContractList(@1, Contract::out, $5, $7, $3);
    }

fundec:
    TT_FUNC TT_ID TT_LPARENS TT_RPARENS TT_EQ contracts expr {
        $$ = new AstFundecNode(@1, $2, nullptr, nullptr, $7, $6);
    }
    | TT_FUNC TT_ID TT_LPARENS tyfields TT_RPARENS TT_EQ contracts expr {
        $$ = new AstFundecNode(@1, $2, nullptr, $4, $8, $7);
    }
    | TT_FUNC TT_ID TT_LPARENS TT_RPARENS TT_COLON TT_ID TT_EQ contracts expr {
        $$ = new AstFundecNode(@1, $2, $6, nullptr, $9, $8);
    }
    | TT_FUNC TT_ID TT_LPARENS tyfields TT_RPARENS TT_COLON TT_ID TT_EQ contracts expr {
        $$ = new AstFundecNode(@1, $2, $7, $4, $10, $9);
    }
    ;

let_expr:
    TT_LET decs TT_IN TT_END {
        $$ = new AstLetNode(@1, $2, nullptr);
    }
    | TT_LET decs TT_IN exprs TT_END {
        $$ = new AstLetNode(@1, $2, $4);
    }
    ;

int_expr:
    expr TT_PLUS expr {
        $$ = new AstBinopNode(@1, BinOp::plus, $1, $3);
    }
    | expr TT_MINUS expr {
        $$ = new AstBinopNode(@1, BinOp::minus, $1, $3);
    }
    | expr TT_MUL expr {
        $$ = new AstBinopNode(@1, BinOp::mul, $1, $3);
    }
    | expr TT_DIV expr {
        $$ = new AstBinopNode(@1, BinOp::div, $1, $3);
    }
    | expr TT_EQ expr {
        $$ = new AstBinopNode(@1, BinOp::eq, $1, $3);
    }
    | expr TT_NEQ expr {
        $$ = new AstBinopNode(@1, BinOp::neq, $1, $3);
    }
    | expr TT_LT expr {
        $$ = new AstBinopNode(@1, BinOp::lt, $1, $3);
    }
    | expr TT_LE expr {
        $$ = new AstBinopNode(@1, BinOp::le, $1, $3);
    }
    | expr TT_GT expr {
        $$ = new AstBinopNode(@1, BinOp::gt, $1, $3);
    }
    | expr TT_GE expr {
        $$ = new AstBinopNode(@1, BinOp::ge, $1, $3);
    }
    | expr TT_AND expr {
        $$ = new AstBinopNode(@1, BinOp::aand, $1, $3);
    }
    | expr TT_OR expr {
        $$ = new AstBinopNode(@1, BinOp::oor, $1, $3);
    }
    | TT_MINUS expr %prec UMINUS {
        $$ = new AstUminusNode(@1, $2);
    }
    | TT_LPARENS expr TT_RPARENS {
        $$ = $2;
    }
    ;

assign_expr:
    lval TT_ASSIGN expr {
        $$ = new AstAssignNode(@1, new AstLvalExprNode(@1, $1), $3);
    }
    ;

funcall_args:
    expr {
        $$ = new AstExprList(@1, $1, nullptr);
    }
    | expr TT_COMMA funcall_args {
        $$ = new AstExprList(@1, $1, $3);
    }
    ;

funcall_expr:
    TT_ID TT_LPARENS TT_RPARENS {
        $$ = new AstFuncallNode(@1, $1, nullptr);
    }
    | TT_ID TT_LPARENS funcall_args TT_RPARENS {
        $$ = new AstFuncallNode(@1, $1, $3);
    }
    ;

exprseq_expr:
    TT_LPARENS expr TT_SEMICOLON exprs TT_RPARENS {
        $$ = new AstExprSeqNode(@1, new AstExprList(@1, $2, $4));
    }
    ;

lval:
    TT_ID {
        $$ = new AstVarNode(@1, $1);
    }
    | lval TT_DOT TT_ID {
        $$ = new AstRecordNode(@1, $1, $3);
    }
    | lval TT_LBRACKET expr TT_RBRACKET {
        $$ = new AstArrayNode(@1, $1, $3);
    }
    | TT_ID TT_LBRACKET expr TT_RBRACKET {
        $$ = new AstArrayNode(@1, new AstVarNode(@1, $1), $3);
    }
    ;

record_args:
    TT_ID TT_EQ expr {
        $$ = new AstRecordArgsList(@1, $1, $3, nullptr);
    }
    | TT_ID TT_EQ expr TT_COMMA record_args {
        $$ = new AstRecordArgsList(@1, $1, $3, $5);
    }

record_creation:
    TT_ID TT_LBRACE TT_RBRACE {
        $$ = new AstRecordCreationNode(@1, $1, nullptr);
    }
    | TT_ID TT_LBRACE record_args TT_RBRACE {
        $$ = new AstRecordCreationNode(@1, $1, $3);
    }
    ;

array_creation:
    TT_ID TT_LBRACKET expr TT_RBRACKET TT_OF expr {
        $$ = new AstArrayCreationNode(@1, $1, $3, $6);
    }
    ;

cond_expr:
    TT_IF expr TT_THEN expr {
        $$ = new AstIfNode(@1, $2, $4, nullptr);
    }
    | TT_IF expr TT_THEN expr TT_ELSE expr {
        $$ = new AstIfNode(@1, $2, $4, $6);
    }
    ;

while_expr:
    TT_WHILE expr TT_DO expr {
        $$ = new AstWhileNode(@1, $2, $4);
    }
    ;

for_expr:
    TT_FOR TT_ID TT_ASSIGN expr TT_TO expr TT_DO expr {
        $$= new AstForNode(@1, $2, $4, $6, $8);
    }
    ;

%%

void yyerror(char *s, ...)
{
    va_list ap;
    va_start(ap, s);

    //fprintf(stderr, "[ERRO NA LINHA %d]: ", yylineno);
    vfprintf(stderr, s, ap);
    fprintf(stderr, "\n");
    abort();
}
