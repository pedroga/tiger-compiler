#pragma once

#include <unordered_map>
#include <vector>
#include <utility>
#include <cstdio>
#include <cstring>
#include <cassert>

/* BASE ENTRY ABSTRACT TYPE
******************************/
struct Entry {
    virtual ~Entry() = default;
    virtual const char *get_name() const = 0;
    int generation;
};

/*******************************************************************************
 * VARIABLES AND FUNCTIONS ENVIRONMENT ENTRIES
 ******************************************************************************/
struct ValEntry : Entry {
    virtual const char *get_name() const = 0;
    virtual const char *get_type() const = 0;
    virtual void print() const = 0;
};

struct VarEntry : ValEntry {
    VarEntry(const char *name, const char *type) : name(name), type(type) {}
    const char *get_name() const {
        return this->name;
    }
    const char *get_type() const {
        return this->type;
    }
    void print() const {
        printf("VarEntry: [%s] -> [%s]\n", this->name, this->type);
    }
private:
    const char *name, *type;
};

// Used by for iterator.
struct ConstEntry : ValEntry {
    ConstEntry(const char *name, const char *type) : name(name), type(type) {}
    const char *get_name() const {
        return this->name;
    }
    const char *get_type() const {
        return this->type;
    }
    void print() const {
        printf("ConstEntry: [%s] -> [%s]\n", this->name, this->type);
    }
private:
    const char *name, *type;
};


typedef std::pair<const char *, const char *> TypePair;

typedef std::vector<TypePair> ParamsVec;
struct FunEntry : ValEntry {
    FunEntry(
        const char *name, const char *type, ParamsVec *params, bool is_head = false)
        : name(name), type(type), params(params), is_head_(is_head) {}
    const char *get_name() const {
        return this->name;
    }
    const char *get_type() const {
        return this->type;
    }
    const ParamsVec *get_params() const {
        return this->params;
    }
    const bool is_head() const {
        return this->is_head_;
    }
    void print() const {
        if (this->is_head_)
            printf("FunHeadEntry: [%s] -> [%s]\n", this->name, this->type);
        else
            printf("FunEntry: [%s] -> [%s]\n", this->name, this->type);
    }
private:
    const char *name, *type;
    const ParamsVec *params;
    const bool is_head_;
    // TODO: contracts.
};

/*******************************************************************************
 * TYPES ENVIRONMENT ENTRIES
 ******************************************************************************/
struct TyEntry : Entry {
    virtual const char *get_name() const = 0;
    virtual void print() const = 0;
};

struct TyHeadEntry : TyEntry {
    TyHeadEntry(const char *name) : name(name) {}
    const char *get_name() const {
        return this->name;
    }
    void print() const {
        printf("TyHeadEntry: [%s] \n", this->name);
    }
private:
    const char *name;
};

struct BaseTyEntry : public TyEntry {
    BaseTyEntry(const char *type) : type(type) {}
    const char *get_name() const {
        return this->type;
    }
    void print() const {
        printf("BaseTyEntry: [%s] \n", this->type);
    }
private:
    const char *type;
};

struct AliasEntry : public TyEntry {
    AliasEntry(const char *name, const char *type) : name(name), type(type) {}
    const char *get_name() const {
        return this->name;
    }
    const char *get_type() const {
        return this->type;
    }
    void print() const {
        printf("AliasEntry: [%s] -> [%s]\n", this->name, this->type);
    }
private:
    const char *name, *type;
};

typedef std::vector<TypePair> FieldsVec;
struct RecordEntry : public TyEntry {
    RecordEntry(const char *name, FieldsVec *fields)
        : name(name), fields(fields) { assert(this->name); assert(this->fields); }
    const char *get_name() const {
        return this->name;
    }
    const FieldsVec *get_fields() const {
        return this->fields;
    }
    void print() const {
        printf("RecordEntry: [%s]\n", this->name);
        for (auto it = this->fields->begin(); it != this->fields->end(); ++it) {
            printf("  .[%s] -> [%s]\n", it->first, it->second);
        }
    }
private:
    const char *name;
    // TODO: delete fields when entry is deleted.
    const FieldsVec *fields;
};

struct ArrayEntry : public TyEntry {
    ArrayEntry(const char *name, const char *type) : name(name), type(type) {}
    const char *get_name() const {
        return this->name;
    }
    const char *get_type() const {
        return this->type;
    }
    void print() const {
        printf("ArrayEntry: [%s] array of [%s]\n", this->name, this->type);
    }
private:
    const char *name, *type;
};

/*******************************************************************************
 * ENVIRONMENT
 ******************************************************************************/
template <typename EntryT>
struct Environment {
    Environment();
    void enter();
    void leave();
    // Prints every entry in the top scope of the environment.
    void print();
    // Returns true if name already existed with same gen and was shadowed.
    bool add(const char *id, EntryT *entry, int gen);
    EntryT *get(const char *id);
private:
    std::vector<std::unordered_map<std::string, EntryT *>> env;
};

/*
 * There are two environments active at all times, the val environment,
 * and the types environment:
 *   tyenv contains the primitive types: int, string, nil and void;
 *   valenv contains the built-in functions: print, flush, getchar, etc.
 */
extern Environment<ValEntry> g_valenv;
extern Environment<TyEntry> g_tyenv;

template <> inline
Environment<ValEntry>::Environment()
{
    std::unordered_map<std::string, ValEntry *> table;

    {// function print(s: string)
        auto print_entry =
            new FunEntry("print", "0", new ParamsVec(1, TypePair("s", "string")));
        table.insert({"print", print_entry});
    }
    {// function printd(i: int)
        auto printd_entry =
            new FunEntry("printd", "0", new ParamsVec(1, TypePair("i", "int")));
        table.insert({"printd", printd_entry});
    }
    {// function flush()
        auto flush_entry = new FunEntry("flush", "0", new ParamsVec());
        table.insert({"flush", flush_entry});
    }
    {// function getchar(): string
        auto getchar_entry = new FunEntry("getchar", "string", new ParamsVec());
        table.insert({"getchar", getchar_entry});
    }
    {// function ord(s: string): int
        auto ord_entry =
            new FunEntry("ord", "int", new ParamsVec(1, TypePair("s", "string")));
        table.insert({"ord", ord_entry});
    }
    {// function chr(i: int): string
        auto chr_entry =
            new FunEntry("chr", "string", new ParamsVec(1, TypePair("i", "int")));
        table.insert({"chr", chr_entry});
    }
    {// function size(s: string): int
        auto size_entry =
            new FunEntry("size", "int", new ParamsVec(1, TypePair("s", "string")));
        table.insert({"size", size_entry});
    }
    {// function substring(s: string, first: int, n: int): string
        auto substring_params = new ParamsVec();
        substring_params->push_back({"s", "string"});
        substring_params->push_back({"first", "int"});
        substring_params->push_back({"n", "int"});
        auto substring_entry = new FunEntry("substring", "string", substring_params);
        table.insert({"substring", substring_entry});
    }
    {// function concat(s1: string, s2: string): string
        auto concat_params = new ParamsVec();
        concat_params->push_back({"s1", "string"});
        concat_params->push_back({"s2", "string"});
        auto concat_entry = new FunEntry("concat", "string", concat_params);
        table.insert({"concat", concat_entry});
    }
    {// function not(i: int): int
        auto not_entry =
            new FunEntry("not", "int", new ParamsVec(1, TypePair("i", "int")));
        table.insert({"not", not_entry});
    }
    {// function exit(i: int)
        auto exit_entry =
            new FunEntry("exit", "0", new ParamsVec(1, TypePair("i", "int")));
        table.insert({"exit", exit_entry});
    }

    this->env.push_back(table);
}

template <> inline
Environment<TyEntry>::Environment()
{
    std::unordered_map<std::string, TyEntry *> table;

    // Primitive types.
    table["int"] = new BaseTyEntry("int");
    table["string"] = new BaseTyEntry("string");
    table["nil"] = new BaseTyEntry("nil");
    table["0"] = new BaseTyEntry("0"); // void type.

    this->env.push_back(table);
}


template <typename EntryT> inline
void Environment<EntryT>::enter()
{
    std::unordered_map<std::string, EntryT *> table;
    this->env.push_back(table);
}

template <typename EntryT> inline
void Environment<EntryT>::leave()
{
    assert(this->env.size() > 1);
    // TODO: free entries in the popped table.
    this->env.pop_back();
}

template <typename EntryT> inline
void Environment<EntryT>::print()
{
    for (int i = env.size() - 1; i >= 0; --i) {
        auto& env = this->env[i];
        printf("Env [%d]\n", i);
        puts("--");
        for (auto it = env.begin(); it != env.end(); ++it) {
            assert(it->second);
            it->second->print();
        }
        puts("--");
    }
}

template <> inline
bool Environment<ValEntry>::add(const char *id, ValEntry *entry, int gen)
{
    assert(id);
    assert(!this->env.empty());
    auto& env = this->env.back();
    auto it = env.find(id);
    bool replaced_entry;

    entry->generation = gen;
    if (it == env.end()) {
        env.insert({id, entry});
        replaced_entry = false;
    } else {
        // TODO: delete entry
        if (it->second->generation == gen)
            replaced_entry = true;
        it->second = entry;
    }

    return replaced_entry;
}

template <> inline
bool Environment<TyEntry>::add(const char *id, TyEntry *entry, int gen)
{
    assert(id);
    assert(!this->env.empty());
    auto& env = this->env.back();
    auto it = env.find(id);
    bool replaced_entry;

    entry->generation = gen;
    if (it == env.end()) {
        env.insert({id, entry});
        replaced_entry = false;
    } else {
        // TODO: delete entry
        if (it->second->generation == gen)
            replaced_entry = true;
        it->second = entry;
    }

    {// Check if a cycle was introduced.
        auto alias = dynamic_cast<AliasEntry *>(entry);
        if (alias) {
            const char *name = alias->get_name();
            auto type_entry = g_tyenv.get(alias->get_type());

            AliasEntry *type_alias = nullptr;
            while ((type_alias = dynamic_cast<AliasEntry *>(type_entry))) {
                const char *alias_type = type_alias->get_type();
                if (!strcmp(alias_type, name)) {
                    fprintf(stderr, "Nao eh permitido ciclo de tipos\n");
                    exit(1);
                }
                type_entry = g_tyenv.get(alias_type);
            }
        }
    }

    return replaced_entry;
}

template <typename EntryT> inline
EntryT *Environment<EntryT>::get(const char *id)
{
    assert(id);
    for (auto it = this->env.rbegin(); it != this->env.rend(); ++it) {
        auto entry = it->find(id);
        if (entry != it->end()) {
            assert(entry->second);
            return entry->second;
        }
    }
    return nullptr;
}

inline const char *translate_type(const char *name)
{
    const char *result = name;
    for (auto entry = g_tyenv.get(result); entry; entry = g_tyenv.get(result)) {
        auto alias = dynamic_cast<AliasEntry *>(entry);
        if (alias) {
            result = alias->get_type();
        } else {
            break;
        }
    }
    return result;
}

inline const char *type_name(const Entry *entry)
{
    assert(entry);
    auto valEntry = dynamic_cast<const ValEntry *>(entry);
    if (valEntry) {// If condition is a ValEntry, it's type must be int.
        return valEntry->get_type();
    }
    auto tyEntry = dynamic_cast<const TyEntry *>(entry);
    if (tyEntry) {
        return tyEntry->get_name();
    }
    assert(0);
}
