%option noyywrap
%Start STATE_INITIAL STATE_COMMENT STATE_STRING
%{
#include <cstdio>
#include <cstring>
#include "parser.hh"

#define BUFFSIZE 255

int buffer_len;
char buffer[BUFFSIZE + 1];

int comment_level = 0;

int yycolumn = 0, yyline = 1;

#define tabstop 8

#define YY_USER_ACTION                                                          \
    yylloc.first_line = yylloc.last_line = yyline;                              \
    yylloc.first_column = yycolumn; yylloc.last_column = yycolumn + yyleng - 1; \
    yycolumn += yyleng;

%}

digit [0-9]
alpha [a-zA-Z]
number {digit}+
identifier {alpha}("_"|{alpha}|{digit})*
punctuation [' ''\n''\t'';'':'',''.''\'''"''+''-''*''/''('')''{''}''<''>''&''|''=''\[''\]']

%%

<STATE_INITIAL>"type" { return TT_TYPE; }
<STATE_INITIAL>"array" { return TT_ARRAY; }
<STATE_INITIAL>"of" {return TT_OF; }
<STATE_INITIAL>"var" {return TT_VAR; }
<STATE_INITIAL>"function" {return TT_FUNC; }
<STATE_INITIAL>"nil" {return TT_NIL; }
<STATE_INITIAL>"if" {return TT_IF; }
<STATE_INITIAL>"then" {return TT_THEN; }
<STATE_INITIAL>"else" {return TT_ELSE; }
<STATE_INITIAL>"while" {return TT_WHILE; }
<STATE_INITIAL>"for" {return TT_FOR; }
<STATE_INITIAL>"to" {return TT_TO; }
<STATE_INITIAL>"do" {return TT_DO; }
<STATE_INITIAL>"break" {return TT_BREAK; }
<STATE_INITIAL>"let" {return TT_LET; }
<STATE_INITIAL>"in" {return TT_IN; }
<STATE_INITIAL>"out" {return TT_OUT; }
<STATE_INITIAL>"end" {return TT_END; }

<STATE_INITIAL>"," {return TT_COMMA; }
<STATE_INITIAL>":" {return TT_COLON; }
<STATE_INITIAL>";" {return TT_SEMICOLON; }
<STATE_INITIAL>"(" {return TT_LPARENS; }
<STATE_INITIAL>")" {return TT_RPARENS; }
<STATE_INITIAL>"[" {return TT_LBRACKET; }
<STATE_INITIAL>"]" {return TT_RBRACKET; }
<STATE_INITIAL>"{" {return TT_LBRACE; }
<STATE_INITIAL>"}" {return TT_RBRACE; }
<STATE_INITIAL>"." {return TT_DOT; }
<STATE_INITIAL>"+" {return TT_PLUS; }
<STATE_INITIAL>"-" {return TT_MINUS; }
<STATE_INITIAL>"*" {return TT_MUL; }
<STATE_INITIAL>"/" {return TT_DIV; }
<STATE_INITIAL>"=" {return TT_EQ; }
<STATE_INITIAL>"<>" {return TT_NEQ; }
<STATE_INITIAL>"<" {return TT_LT; }
<STATE_INITIAL>">" {return TT_GT; }
<STATE_INITIAL>"<=" {return TT_LE; }
<STATE_INITIAL>">=" {return TT_GE; }
<STATE_INITIAL>":=" {return TT_ASSIGN; }
<STATE_INITIAL>"&" {return TT_AND; }
<STATE_INITIAL>"|" {return TT_OR; }
<STATE_INITIAL>" " { }
<STATE_INITIAL>"\t" { yycolumn += tabstop; }
<STATE_INITIAL>"\n" { yycolumn = 0; ++yyline; }
<STATE_INITIAL><<EOF>> { return TT_EOF; }

<STATE_INITIAL>{identifier} {
    yylval.str = strdup(yytext);
    return TT_ID;
}

<STATE_INITIAL>{number} { yylval.num = atoi(yytext); return TT_INTEGER; }

<STATE_INITIAL>"\"" {
    BEGIN STATE_STRING;
    yylval.str = NULL;
    buffer[0] = '\0';
    buffer_len = 0;
}

<STATE_INITIAL>. {
    fprintf(stderr, "[ERRO LEXICO - %d:%d] token invalido.\n",
            yylloc.first_line, yylloc.first_column);
    exit(1);  }

<STATE_STRING>"\\a" {
    buffer[buffer_len++] = '\a';
}
<STATE_STRING>"\\b" {
    buffer[buffer_len++] = '\b';
}
<STATE_STRING>"\\f" {
    buffer[buffer_len++] = '\f';
}
<STATE_STRING>"\\n" {
    buffer[buffer_len++] = '\n';
}
<STATE_STRING>"\\r" {
    buffer[buffer_len++] = '\r';
}
<STATE_STRING>"\\t" {
    buffer[buffer_len++] = '\t';
}
<STATE_STRING>"\\v" {
    buffer[buffer_len++] = '\v';
}
<STATE_STRING>"\\\\" {
    buffer[buffer_len++] = '\\';
}
<STATE_STRING>"\\\"" {
    buffer[buffer_len++] = '\"';
}
<STATE_STRING>"\t" { yycolumn += tabstop; }
<STATE_STRING>"\n" {
    fprintf(stderr, "[ERRO LEXICO - %d:%d] String nao terminada.\n",
            yylloc.first_line, yylloc.first_column);
    exit(1);
}
<STATE_STRING><<EOF>> {
    fprintf(stderr, "[ERRO LEXICO - %d:%d] String nao terminada.\n",
            yylloc.first_line, yylloc.first_column);
    exit(1);
}
<STATE_STRING>"\\" {
    fprintf(stderr, "[ERRO LEXICO - %d:%d] Sequencia de escape nao reconhecida.\n",
            yylloc.first_line, yylloc.first_column);
    exit(1);
}
<STATE_STRING>"\"" {
    BEGIN STATE_INITIAL;
    buffer[buffer_len] = '\0';
    yylval.str = strdup(buffer);
    return TT_STRING;
}
<STATE_STRING>. {
    if (buffer_len < BUFFSIZE) {
        buffer[buffer_len++] = yytext[0];
    } else {
        fprintf(stderr, "[ERRO LEXICO - %d:%d] String maior do que o tamanho maximo permitido (%d).\n",
                yylloc.first_line, yylloc.first_column, BUFFSIZE);
        exit(1);
    }
}

<STATE_INITIAL>"/*" { ++comment_level; BEGIN STATE_COMMENT; }
<STATE_COMMENT>"/*" { ++comment_level; }
<STATE_COMMENT>"*/" { if (--comment_level == 0) { BEGIN STATE_INITIAL; } }
<STATE_COMMENT><<EOF>> {
    fprintf(stderr, "[ERRO LEXICO - %d:%d] Comentario nao terminado.\n",
            yylloc.first_line, yylloc.first_column);
    exit(1);
}
<STATE_COMMENT>"\n" { yycolumn = 0; ++yyline; }
<STATE_COMMENT>"\t" { yycolumn += tabstop; }
<STATE_COMMENT>. {}

. { BEGIN STATE_INITIAL; yyless(0); }
<<EOF>> { return TT_EOF; }


%%
