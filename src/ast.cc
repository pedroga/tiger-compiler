#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <stack>
#include <vector>
#include <typeinfo>

#include "ast.hh"

#define indent() {for (int i = 0; i < indent_level; putchar(' '), ++i);}
#define indentp(plus) {for (int i = 0; i < indent_level + (plus); putchar(' '), ++i);}

#define semantic_error(msg)                             \
    fprintf(stderr, "[Erro Semantico %d:%d] %s\n", this->loc.first_line, this->loc.first_column, msg);

// Amount of nested loops, used to check if break is inside a loop.
// It is a stack, because a break cannot break a loop inside another function,
// so every time a fundec is called, a zero is stacked.
static std::stack<bool> nested_loops;

/*********************************************************************
 * AST
 *********************************************************************/

/* LISTS OF NODES
********************/
void AstDecList::print(int indent_level) const
{
    assert(this->next ? this->dec != NULL : true);
    if (this->dec) {
        this->dec->print(indent_level);
    }
    if (this->next) {
        this->next->print(indent_level);
    }
}

void AstExprList::print(int indent_level) const
{
    assert(this->next ? this->expr != NULL : true);
    if (this->expr) {
        this->expr->print(indent_level);
    }
    if (this->next) {
        this->next->print(indent_level);
    }
}

void AstContractList::print(int indent_level) const
{
    assert(!(this->kind == Contract::in && this->id));
    indent();
    if (this->kind == Contract::in) {
        puts("In expr:");
    } else { // Contract::out
        if (this->id)
            printf("Out expr: %s\n", this->id);
        else
            puts("Out expr:");
    }
    this->expr->print(indent_level + 2);
    if (this->next)
        this->next->print(indent_level);
}

/*********************************************************************
 * DECLARATION NODES
 *********************************************************************/

/* TYPE DECLARATION NODES
****************************/
void AstTydecNode::print(int indent_level) const
{   indent();
    printf("Type: %s\n", this->name);
    this->ty->print(indent_level + 2);
}

Entry *AstTydecNode::traverse()
{
    {// Alias declaration.
        auto alias = dynamic_cast<const AstTyAliasNode *>(this->ty);
        if (alias) {
            if (!g_tyenv.get(alias->name)) {
                fprintf(stderr, "[ERRO SEMANTICO] "
                        "o tipo %s nao existe\n", alias->name);
                exit(1);
            }
            auto entry = new AliasEntry(this->name, alias->name);
            return entry;
        }
    }
    {// Record declaration.
        auto record = dynamic_cast<const AstTyRecordNode *>(this->ty);
        if (record) {
            auto vec = new FieldsVec();
            for (auto field = record->fields; field; field = field->next) {
                assert(field->id && field->type);
                if (!g_tyenv.get(field->type)) {
                    char msg[255] = "";
                    snprintf(msg, 255, "O tipo [%s] nao existe neste escopo",
                             field->type);
                    semantic_error(msg);
                    exit(1);
                }
                for (auto it = vec->begin(); it != vec->end(); ++it) {
                    if (!strcmp(it->first, field->id)) {
                        fprintf(stderr, "O campo [%s] ja foi declarado neste "
                                "registro\n", field->id);
                        exit(1);
                    }
                }

                vec->push_back({field->id, field->type});
            }
            auto entry = new RecordEntry(this->name, vec);
            return entry;
        }
    }
    {// Array declaration.
        auto array = dynamic_cast<const AstTyArrayNode *>(this->ty);
        if (array) {
            if (!g_tyenv.get(array->name)) {
                fprintf(stderr, "[ERRO SEMANTICO] "
                        "o tipo %s nao existe\n", array->name);
                exit(1);
            }
            auto entry = new ArrayEntry(this->name, array->name);
            return entry;
        }
    }
    {// Error.
        fprintf(stderr, "[ERRO SEMANTICO] declaracao de tipo invalida\n");
        exit(1);
    }
}

Entry *AstTydecNode::traverse_head() const
{
    return new TyHeadEntry(this->name);
}

void AstTyAliasNode::print(int indent_level) const
{   indent();
    printf("Alias of: %s\n", this->name);
}

void AstTyRecordFieldNode::print(int indent_level) const
{   indent();
    printf("Tyfield: %s -> %s\n", this->id, this->type);
    if (this->next)
        this->next->print(indent_level);
}

void AstTyRecordNode::print(int indent_level) const
{   indent();
    printf("Record: {");
    if (this->fields) {
        puts("");
        this->fields->print(indent_level + 2);
        indent();
    }
    printf("}\n");
}

void AstTyArrayNode::print(int indent_level) const
{   indent();
    printf("Array of: %s\n", this->name);
}

/* VAR DECLARATION NODE
**************************/
void AstVardecNode::print(int indent_level) const
{   indent();
    if (this->type)
        printf("Var %s -> %s =\n", this->name, this->type);
    else
        printf("Var %s -> [inferred] =\n", this->name);
    this->value->print(indent_level + 2);
}

Entry *AstVardecNode::traverse()
{
    auto rhs_type = this->value->traverse();
    {// Verify var and rhs types.
        if (this->type && !g_tyenv.get(this->type)) {// The type must be declared.
            char msg[255] = "";
            snprintf(msg, 255, "Tipo [%s] nao declarado\n", this->type);
            semantic_error(msg);
            exit(1);
        }
        if (!rhs_type) {// rhs expr must have some known type.
            semantic_error("expr de tipo desconhecido\n");
            exit(1);
        }
        // nil cannot be assigned to inferred var.
        if (!this->type && !strcmp(rhs_type->get_name(), "nil")) {
            semantic_error("nao eh possivel inferir o tipo de nil");
            exit(1);
        }
        if (this->type) {// If the type is not inferred, it must match the expr type.
            // nil can only be assigned to record types.
            if (!strcmp(rhs_type->get_name(), "nil")) {
                auto type_entry = g_tyenv.get(this->type);
                if (!dynamic_cast<RecordEntry *>(type_entry)) {
                    semantic_error("Nil soh pode ser atribuido em um registro\n");
                    exit(1);
                }
            } else if (strcmp(rhs_type->get_name(), translate_type(this->type))) {
                char msg[255] = "";
                snprintf(msg, 255, "Esperava expr do tipo %s "
                        "porem a expr eh do tipo %s", this->type, rhs_type->get_name());
                semantic_error(msg);
                exit(1);
            }
        }
    }
    {// The types are ok, try to add to g_valenv
        auto entry = this->type ?
            new VarEntry(this->name, this->type) :
            new VarEntry(this->name, rhs_type->get_name());
        return entry;
    }
}

Entry *AstVardecNode::traverse_head() const
{
    return nullptr;
}

/* FUNCTION DECLARATION NODE
*******************************/
void AstFundecNode::print(int indent_level) const
{   indent();
    if (this->type) {
        printf("Function %s -> %s:\n", this->name, this->type);
    } else {
        printf("Procedure %s:\n", this->name);
    }
    if (this->params) {
        indentp(2);
        printf("Params:\n");
        this->params->print(indent_level + 4);
    }
    if (this->contracts) {
        indentp(2);
        puts("Contracts:");
        this->contracts->print(indent_level + 4);
    }
    if (this->expr) {
        indentp(2);
        printf("Expr:\n");
        this->expr->print(indent_level + 4);
    }
}

Entry *AstFundecNode::traverse()
{
    // A break cannot break a loop of an outter function.
    nested_loops.push(false);

    g_valenv.enter();
    auto params = new ParamsVec();
    {// Add the params for evaluating the expr in the entered scope.
        int idx = 0;
        for (auto param = this->params; param; param = param->next, ++idx) {
            // Check if the type of the param exists.
            if (!g_tyenv.get(param->type)) {
                fprintf(stderr, "O tipo do parametro %d nao existe\n", idx);
                exit(1);
            }

            auto varEntry = new VarEntry(param->id, translate_type(param->type));
            if (g_valenv.add(param->id, varEntry, 0)) {
                fprintf(stderr, "Nao podem haver dois parametros com o mesmo nome\n");
                exit(1);
            }
            params->push_back({param->id, translate_type(param->type)});
        }
    }
    {// Verify contracts.
        for (auto contract = this->contracts; contract; contract = contract->next) {
            switch (contract->kind) {
            case Contract::in: {
                assert(!contract->id);
                auto expr_type = translate_type(type_name(contract->expr->traverse()));
                if (strcmp(expr_type, "int")) {
                    fprintf(stderr, "A expr deve ser do tipo int\n");
                    exit(1);
                }
                break;
            }
            case Contract::out: {
                g_valenv.enter();
                if (contract->id) {
                    if (!this->type) {
                        fprintf(stderr, "Procedimentos nao podem ter declaracao no out\n");
                        exit(1);
                    }
                    auto result_entry = new VarEntry(contract->id, this->type);
                    g_valenv.add(contract->id, result_entry, 0);
                }
                auto expr_type = translate_type(type_name(contract->expr->traverse()));
                if (strcmp(expr_type, "int")) {
                    fprintf(stderr, "A expr deve ser do tipo int\n");
                    exit(1);
                }
                g_valenv.leave();
                break;
            }}
        }
    }

    // Before traversing the body, verify if the return type exists.
    if (this->type && !g_tyenv.get(translate_type(this->type))) {
        fprintf(stderr, "O tipo da funcao [%s] nao existe!\n", this->type);
        exit(1);
    }

    auto expr_entry = this->expr->traverse();
    assert(expr_entry);

    {// Verify if return type matches expr type.
        auto expr_type = translate_type(type_name(expr_entry));
        if (this->type) {// Function, verify if expr type matches return type.
            if (strcmp(translate_type(this->type), expr_type)) {
                fprintf(stderr, "O tipo de retorno [%s] e da expr [%s] nao batem\n",
                        translate_type(this->type), expr_type);
                exit(1);
            }
        } else {// Procedure, expr type must be no-value (0).
            if (strcmp("0", expr_type)) {
                char msg[255] = "";
                snprintf(msg, 255, "A expr de um procedimento nao pode ter um tipo, "
                         "porem eh [%s]", expr_type);
                semantic_error(msg);
                exit(1);
            }
        }
    }

    g_valenv.leave();
    auto entry = new FunEntry(this->name, this->type, params);

    nested_loops.pop();
    return entry;
}

Entry *AstFundecNode::traverse_head() const
{
    auto params = new ParamsVec();
    {// Verify if params are valid.
        int idx = 0;
        for (auto param = this->params; param; param = param->next, ++idx) {
            if (!g_tyenv.get(translate_type(param->type))) {
                fprintf(stderr, "O tipo do parametro %d nao existe\n", idx);
                exit(1);
            }
            params->push_back({param->id, translate_type(param->type)});
        }
    }
    return new FunEntry(this->name, this->type, params, true);
}

/*********************************************************************
 * L-VALUES
 *********************************************************************/
void AstVarNode::print(int indent_level) const
{   indent();
    printf("Simple var: %s\n", this->name);
}

Entry *AstVarNode::traverse()
{
    // TODO: verify if not a function.
    auto entry = g_valenv.get(this->name);
    if (!entry) {
        char msg[255] = "";
        snprintf(msg, 255, "A variavel [%s] nao foi declarada", this->name);
        semantic_error(msg);
        exit(1);
    }
    return entry;
}

void AstRecordNode::print(int indent_level) const
{   indent();
    puts("Record:");
    indentp(2);
    puts("Lval:");
    this->lval->print(indent_level + 4);
    indentp(2);
    puts("Field:");
    indentp(4);
    printf("%s\n", this->field);
}

Entry *AstRecordNode::traverse()
{
    auto entry = this->lval->traverse();
    auto tyEntry = g_tyenv.get(translate_type(type_name(entry)));
    if (tyEntry) {
        auto recEntry = dynamic_cast<RecordEntry *>(tyEntry);
        if (recEntry) {
            this->type = strdup(recEntry->get_name());
            auto& fields = *recEntry->get_fields();
            int idx = 0;
            for (auto it = fields.begin(); it != fields.end(); ++it, ++idx) {
                if (!strcmp(it->first, this->field)) {
                    this->idx = idx;
                    return g_tyenv.get(it->second);
                }
            }
            char msg[255] = "";
            snprintf(msg, 255, "O campo [%s] nao faz parte do registro [%s]",
                    this->field, recEntry->get_name());
            semantic_error(msg);
            exit(1);
        }
    }
    semantic_error("A variavel nao eh um registro");
    exit(1);
}

void AstArrayNode::print(int indent_level) const
{   indent();
    puts("Array:");
    this->lval->print(indent_level + 4);
    indentp(2);
    puts("Idx:");
    this->idx->print(indent_level + 4);
}

Entry *AstArrayNode::traverse()
{
    auto entry = this->lval->traverse();
    auto tyEntry = g_tyenv.get(translate_type(type_name(entry)));
    if (tyEntry) {
        auto arrayEntry = dynamic_cast<ArrayEntry *>(tyEntry);
        if (arrayEntry) {
            return g_tyenv.get(arrayEntry->get_type());
        }
    }
    semantic_error("A variavel nao eh um array");
    exit(1);
}

/*********************************************************************
 * EXPRESSION NODES
 *********************************************************************/

/* BUILT-IN TYPE NODES
*************************/
void AstIntegerNode::print(int indent_level) const
{   indent();
    printf("Integer: %d\n", this->val);
}

Entry *AstIntegerNode::traverse()
{
    return g_tyenv.get("int");
}

void AstStringNode::print(int indent_level) const
{   indent();
    printf("String: %s\n", this->val);
}

Entry *AstStringNode::traverse()
{
    return g_tyenv.get("string");
}

void AstNilNode::print(int indent_level) const
{   indent();
    printf("Nil\n");
}

Entry *AstNilNode::traverse()
{
    return g_tyenv.get("nil");
}

Entry *AstNovalNode::traverse()
{
    return g_tyenv.get("0");
}

void AstBreakNode::print(int indent_level) const
{   indent();
    printf("Break\n");
}

Entry *AstBreakNode::traverse()
{
    if (nested_loops.empty() || !nested_loops.top()) {
        fprintf(stderr, "Break sem um loop correspondente\n");
        exit(1);
    }
    return g_tyenv.get("0");
}

/* UNARY OPERATORS
*********************/
void AstUminusNode::print(int indent_level) const
{   indent();
    puts("Uminus:");
    this->child->print(indent_level + 2);
}

Entry *AstUminusNode::traverse()
{
    auto entry = this->child->traverse();
    if (strcmp(translate_type(type_name(entry)), "int")) {
        fprintf(stderr, "Esperava expr do tipo int, porem a expr eh do tipo [%s]\n",
                translate_type(type_name(entry)));
        exit(1);
    }
    return entry;
}

/* BINARY OPERATORS
**********************/
void AstBinopNode::print(int indent_level) const
{   indent();
    switch (this->op) {
    case BinOp::plus: puts("Plus:"); break;
    case BinOp::minus: puts("Minus:"); break;
    case BinOp::mul: puts("Mul:"); break;
    case BinOp::div: puts("Div:"); break;
    case BinOp::eq: puts("Equal:"); break;
    case BinOp::neq: puts("Not equal:"); break;
    case BinOp::lt: puts("Less than:"); break;
    case BinOp::le: puts("Less or equal:"); break;
    case BinOp::gt: puts("Greater than:"); break;
    case BinOp::ge: puts("Greater or equal:"); break;
    case BinOp::aand: puts("And:"); break;
    case BinOp::oor: puts("Or:"); break;
    }
    this->left->print(indent_level + 2);
    this->right->print(indent_level + 2);
}

Entry *AstBinopNode::traverse()
{
    auto leftType = translate_type(type_name(this->left->traverse()));
    auto rightType = translate_type(type_name(this->right->traverse()));

    switch (this->op) {
    case BinOp::plus: case BinOp::minus:
    case BinOp::mul: case BinOp::div:
    case BinOp::aand: case BinOp::oor: {
        if (strcmp(leftType, "int") || strcmp(rightType, "int")) {
            semantic_error("Operacao apenas valida para inteiros");
            exit(1);
        }
        break;
    }
    case BinOp::lt: case BinOp::le:
    case BinOp::gt: case BinOp::ge: {
        bool cond =
            (strcmp(leftType, rightType)) ||
            (strcmp(leftType, "int") && strcmp(leftType, "string"));
        if (cond) {
            semantic_error("Essa comparacao eh apenas possivel entre inteiros "
                           "ou entre strings");
            exit(1);
        }
    }
    case BinOp::eq: case BinOp::neq: {
        // Can compare nil only to record types (or to nil itself).
        auto leftTypeEntry = g_tyenv.get(leftType);
        auto rightTypeEntry = g_tyenv.get(rightType);
        bool nil_cmp =
            (!strcmp("nil", leftType) && !dynamic_cast<RecordEntry *>(rightTypeEntry)) ||
            (!strcmp("nil", rightType) && !dynamic_cast<RecordEntry *>(leftTypeEntry));
        if (nil_cmp) {
            fprintf(stderr, "Nil apenas pode ser comparado com registros\n");
            exit(1);
        }
        // If neither is nil, they must be the same type.
        if (strcmp("nil", leftType) && strcmp("nil", rightType) &&
            strcmp(leftType, rightType)) {
            semantic_error("Apenas eh possivel comparar objetos do mesmo tipo");
            exit(1);
        }

        break;
    }}
    return g_tyenv.get("int");
}

/* SPECIAL EXPR NODES
*************************/
void AstLetNode::print(int indent_level) const
{   indent();
    puts("Let:");
    indentp(2);
    puts("Decs:");
    if (this->decs) this->decs->print(indent_level + 4);
    if (this->exprs) {
        indentp(2);
        puts("Exprs:");
        if (this->exprs) this->exprs->print(indent_level + 4);
    }
}

Entry *AstLetNode::traverse()
{
    //puts("TRAVERSING LET NODE");
    g_valenv.enter();
    g_tyenv.enter();

    {//   puts("TRAVERSING DEC LIST");
        enum class DeclarationType {
            var, fun, ty
        };

        std::vector<AstDecNode *> batch;
        DeclarationType batch_type = DeclarationType::var;
        AstDecList *decs = this->decs;
        int batch_gen = 0;
        int line = this->loc.first_line, column = this->loc.first_column;
        auto process_batch =
            [&batch, &batch_type, &batch_gen, line, column]() {
                ++batch_gen;
                for (auto node : batch) {
                    bool redef = false;
                    auto entry = node->traverse();
                    if (dynamic_cast<const AstTydecNode *>(node)) {
                        redef = g_tyenv.add(entry->get_name(),
                                            dynamic_cast<TyEntry *>(entry), batch_gen);
                    } else {
                        redef = g_valenv.add(entry->get_name(),
                                             dynamic_cast<ValEntry *>(entry), batch_gen);
                    }
                    if (redef) {
                        fprintf(stderr, "[Erro Semantico %d:%d] Nao pode redefinir um "
                                "nome no mesmo batch de declaracoes\n", line, column);
                        exit(1);
                    }
                }
                batch.clear();
                ++batch_gen;
            };
        assert(decs);

        while (decs) {
            AstDecNode *dec = decs->dec; assert(dec);
            auto head = dec->traverse_head();

            bool changed = false;
            {// Check if the batch type changed.
                if (head == nullptr) { // vardecs are not recursive.
                    changed = true;
                    batch_type = DeclarationType::var;
                } else if (dynamic_cast<FunEntry *>(head)) {
                    if (batch_type != DeclarationType::fun) changed = true;
                    batch_type = DeclarationType::fun;
                } else if (dynamic_cast<TyHeadEntry *>(head)) {
                    if (batch_type != DeclarationType::ty) changed = true;
                    batch_type = DeclarationType::ty;
                }
            }

            if (changed) {
                process_batch();
            }

            {// Add head entry to respective environment.
                auto fun_head = dynamic_cast<FunEntry *>(head);
                if (fun_head) {
                    g_valenv.add(fun_head->get_name(), fun_head, batch_gen);
                }
                auto ty_head = dynamic_cast<TyHeadEntry *>(head);
                if (ty_head) {
                    g_tyenv.add(ty_head->get_name(), ty_head, batch_gen);
                }
            }

            batch.push_back(dec);
            decs = decs->next;
        };
        process_batch();
    }
/*
    puts("PRINTING TYENV OF LET NODE");
    g_tyenv.print();
    puts("PRINTING VALENV OF LET NODE");
    g_valenv.print();
*/
    Entry *last_expr = nullptr;
    {   //puts("TRAVERSING EXPRS LIST");
        AstExprList *exprs = this->exprs;
        while (exprs) {
            auto expr = exprs->expr;
            assert(expr);
            last_expr = expr->traverse();
            exprs = exprs->next;
        };
    }

    const char *last_expr_type = "0";
    if (last_expr) last_expr_type = translate_type(type_name(last_expr));

    auto result = g_tyenv.get(last_expr_type);
    assert(result);

    g_valenv.leave();
    g_tyenv.leave();

    return result;
}

void AstLvalExprNode::print(int indent_level) const
{   indent();
    puts("LvalExpr:");
    this->lval->print(indent_level + 2);
}

Entry *AstLvalExprNode::traverse()
{
    return this->lval->traverse();
}

void AstAssignNode::print(int indent_level) const
{   indent();
    puts("Assignment:");
    indentp(2);
    puts("Lval:");
    this->lval->print(indent_level + 4);
    indentp(2);
    puts("Expr:");
    this->expr->print(indent_level + 4);
}

Entry *AstAssignNode::traverse()
{
    auto lval_entry = lval->traverse();
    auto lval_type = translate_type(type_name(lval_entry));
    if (dynamic_cast<ConstEntry *>(lval_entry)) {
        fprintf(stderr, "Nao eh possivel atribuir em uma constante\n");
        exit(1);
    }

    auto expr_type = translate_type(type_name(expr->traverse()));
    if (!strcmp(expr_type, "nil") &&
        !dynamic_cast<RecordEntry *>(g_tyenv.get(lval_type))) {
        fprintf(stderr, "Tentando atribuir nil a um lval que n eh um registro\n");
        exit(1);
    }
    if (strcmp(expr_type, "nil") && strcmp(lval_type, expr_type)) {
        char msg[255] = "";
        snprintf(msg, 255, "Tentando atribuir expr do tipo [%s] a um lval do "
                "tipo [%s]", expr_type, lval_type);
        semantic_error(msg);
        exit(1);
    }
    return g_tyenv.get("0");
}

void AstFuncallNode::print(int indent_level) const
{   indent();
    printf("Function call: %s\n", this->name);
    if (this->args) {
        indentp(2);
        puts("Arguments:");
        this->args->print(indent_level + 4);
    }
}

Entry *AstFuncallNode::traverse()
{
    assert(this->name);
    auto func_entry = dynamic_cast<FunEntry *>(g_valenv.get(this->name));
    if (!func_entry) {
        char msg[255] = "";
        snprintf(msg, 255, "A funcao [%s] nao foi declarada", this->name);
        semantic_error(msg);
        exit(1);
    }

    auto& params = *func_entry->get_params();
    auto args = this->args;
    int idx = 0;
    for (auto param = params.begin(); param != params.end();
         ++param, ++idx, args = args->next) {
        if (args) { assert(args->expr);
            auto arg_type = translate_type(type_name(args->expr->traverse()));
            if (strcmp(param->second, arg_type)) {
                char msg[255] = "";
                snprintf(msg, 255, "O tipo do argumento %d [%s] nao bate com o tipo "
                        "do parametro [%s]", idx, arg_type, param->second);
                semantic_error(msg);
                exit(1);
            }
        } else {
            fprintf(stderr, "Numero insuficiente de argumentos, faltam %lu\n",
                    params.size() - idx);
            exit(1);
        }
    }
    if (args) {
        for (; args; args = args->next, ++idx)
            ;
        char msg[255] = "";
        snprintf(msg, 255, "Foram supridos mais argumentos do que a funcao esperava: "
                "recebidos %d - esperados %lu", idx, params.size());
        semantic_error(msg);
        exit(1);
    }
    auto entry = func_entry->get_type() ?
        g_tyenv.get(func_entry->get_type()) :
        g_tyenv.get("0");

    return entry;
}

void AstExprSeqNode::print(int indent_level) const
{   indent();
    puts("Sequencing:");
    assert(this->exprs);
    this->exprs->print(indent_level + 2);
}

Entry *AstExprSeqNode::traverse()
{
    Entry *last_expr = nullptr;
    const AstExprList *exprs = this->exprs;
    while (exprs) {
        auto expr = exprs->expr;
        assert(expr);
        last_expr = expr->traverse();
        exprs = exprs->next;
    };
    if (last_expr)
        return last_expr;
    else
        return g_tyenv.get("0");
}

void AstRecordCreationNode::print(int indent_level) const
{   indent();
    printf("Record creation: %s\n", this->type_name);
    if (this->args) {
        indentp(2);
        puts("Args:");
        this->args->print(indent_level + 4);
    }
}

Entry *AstRecordCreationNode::traverse()
{
    auto tyEntry = g_tyenv.get(this->type_name);
    if (!tyEntry) {
        char msg[255] = "";
        snprintf(msg, 255, "Tipo [%s] nao declarado", this->type_name);
        semantic_error(msg);
        exit(1);
    }
    auto recEntry = dynamic_cast<RecordEntry *>(tyEntry);
    if (!recEntry) {
        char msg[255] = "";
        snprintf(msg, 255, "Tipo [%s] nao eh um registro",
                this->type_name);
        semantic_error(msg);
        exit(1);
    }
    {// Verify if field assignments match entry's fields.
        int idx = 0;
        auto& fields = *recEntry->get_fields();
        auto args = this->args;
        for (auto field = fields.begin(); field != fields.end();
             ++field, ++idx, args = args->next) {
            if (args) { assert(args->expr);
                const char *field_name = field->first;
                assert(field_name);
                if (strcmp(field_name, args->id)) {
                    char msg[255] = "";
                    snprintf(msg, 255, "O campo na pos %d [%s] nao bate com o campo "
                             "declarado [%s]", idx, args->id, field_name);
                    semantic_error(msg);
                    exit(1);
                }
                const char *field_type = translate_type(field->second);
                assert(field_type);
                auto exprEntry = args->expr->traverse();
                assert(exprEntry);
                const char *expr_type = nullptr;
                {// Get expr type.
                    auto valEntry = dynamic_cast<const ValEntry *>(exprEntry);
                    if (valEntry) {
                        expr_type = translate_type(valEntry->get_type());
                    }
                    auto tyEntry = dynamic_cast<const TyEntry *>(exprEntry);
                    if (tyEntry) {
                        expr_type = translate_type(tyEntry->get_name());
                    }
                }
                if (!strcmp(expr_type, "nil") &&
                    !dynamic_cast<RecordEntry *>(g_tyenv.get(field_type))) {
                    semantic_error("Nao eh possivel atribuir nil "
                                   "em um tipo que nao seja um registro");
                    exit(1);
                }
                if (strcmp(expr_type, "nil") && strcmp(field_type, expr_type)) {
                    char msg[255] = "";
                    snprintf(msg, 255, "O campo [%s] eh do tipo [%s], "
                            "porem a expressao eh do tipo [%s]",
                             field_name, field_type, expr_type);
                    semantic_error(msg);
                    exit(1);
                }
            } else {
                char msg[255] = "";
                snprintf(msg, 255, "Numero insuficiente de argumentos, faltam %lu",
                         fields.size() - idx);
                semantic_error(msg);
                exit(1);
            }
        }
        if (args) {
            for (; args; args = args->next, ++idx)
                ;
            char msg[255] = "";
            snprintf(msg, 255, "Foram supridos mais argumentos do que o registro "
                     "esperava: recebidos %d - esperados %lu\n", idx, fields.size());
            semantic_error(msg);
            exit(1);
        }
    }
    return recEntry;
}

void AstRecordArgsList::print(int indent_level) const
{   indent();
    printf("%s =\n", this->id);
    this->expr->print(indent_level + 2);
    if (this->next)
        this->next->print(indent_level);
}

void AstArrayCreationNode::print(int indent_level) const
{   indent();
    printf("Array creation: %s\n", this->name);
    indentp(2);
    puts("Len:");
    this->len_expr->print(indent_level + 4);
    indentp(2);
    puts("Init:");
    this->init_expr->print(indent_level + 4);
}

Entry *AstArrayCreationNode::traverse()
/* this->name [this->len_expr] of this->init_expr
****************************************
* name must be in g_tyenv as an array entry
* len_expr must be integer
* init_expr must be same type type as type-id base type
*/
{
    char emsg[255] = "";
    auto entry = g_tyenv.get(translate_type(this->name));
    if (!entry) {
        snprintf(emsg, 255, "O tipo [%s] nao foi declarado", this->name);
        semantic_error(emsg);
        exit(1);
    }
    auto arrEntry = dynamic_cast<ArrayEntry *>(entry);
    if (!arrEntry) {
        snprintf(emsg, 255, "O tipo [%s] nao eh do tipo array",
                 this->name);
        semantic_error(emsg);
        exit(1);
    }
    auto lenExprEntry = this->len_expr->traverse();
    const char *len_type = translate_type(type_name(lenExprEntry));
    if (!lenExprEntry || strcmp(len_type, "int")) {
        snprintf(emsg, 255, "A expressao de tamanho [%s] nao eh do tipo "
                 "int\n", len_type);
        semantic_error(emsg);
        exit(1);
    }
    auto initExprEntry = this->init_expr->traverse();
    const char *exprType, *arrType;
    arrType = translate_type(arrEntry->get_type());
    if (initExprEntry) exprType = translate_type(type_name(initExprEntry));
    if (!initExprEntry || strcmp(exprType, arrType)) {
        // Nil can be assigned to a record array.
        if (!strcmp(exprType, "nil")) {
            auto recEntry = dynamic_cast<RecordEntry *>(
                g_tyenv.get(arrEntry->get_type()));
            if (recEntry)
                return arrEntry;
        }
        snprintf(emsg, 255, "A expressao de inicializacao [%s] nao bate com "
                 "o tipo do array [%s]", exprType, arrType);
        semantic_error(emsg);
        exit(1);
    }
    return arrEntry;
}

void AstIfNode::print(int indent_level) const
{   indent();
    puts("If:");
    indentp(2);
    puts("Condition:");
    this->cond->print(indent_level + 4);
    indentp(2);
    puts("Then:");
    this->then->print(indent_level + 4);
    if (this->elsee) {
        indentp(2);
        puts("Else:");
        this->elsee->print(indent_level + 4);
    }
}

Entry *AstIfNode::traverse()
{
    assert(this->cond && this->then);

    {// Check if condition is of type int.
        auto condEntry = cond->traverse();
        assert(condEntry);
        const char *condType = type_name(condEntry);
        if (strcmp(translate_type(condType), "int")) {
            char msg[255] = "";
            snprintf(msg, 255, "A condicao deve ser do tipo int, porem eh do tipo [%s]",
                    condType);
            semantic_error(msg);
            exit(1);
        }
    }
    if (this->elsee) {// If-then-else -> then and else must be same type.
        Entry *then_entry = this->then->traverse();
        Entry *else_entry = this->elsee->traverse();
        auto thenType = translate_type(type_name(then_entry));
        auto elseType = translate_type(type_name(else_entry));

        // If one of them is nil and the other a record, then its ok to be diferent.
        if (!strcmp(thenType, "nil") && strcmp(elseType, "nil")) {
            auto res = g_tyenv.get(else_entry->get_name());
            if (res) return res;
            else semantic_error("Tipo invalido");
        }
        if (!strcmp(elseType, "nil") && strcmp(thenType, "nil")) {
            auto res = g_tyenv.get(then_entry->get_name());
            if (res) return res;
            else semantic_error("Tipo invalido");
        }
        if (strcmp(thenType, elseType)) {
            semantic_error("A expressao do then e do else devem ser do mesmo tipo");
            exit(1);
        }
        return g_tyenv.get(thenType);
    } else {// If-then -> then must produce no value.
        auto thenType = translate_type(type_name(this->then->traverse()));
        if (strcmp(thenType, "0")) {
            semantic_error("A expressao do then nao deve produzir valor");
            exit(1);
        }
        return g_tyenv.get("0");
    }
}

void AstWhileNode::print(int indent_level) const
{   indent();
    puts("While:");
    indentp(2);
    puts("Condition:");
    this->cond->print(indent_level + 4);
    indentp(2);
    puts("Expr:");
    this->expr->print(indent_level + 4);
}

Entry *AstWhileNode::traverse()
{
    nested_loops.push(true);
    auto cond_type = translate_type(type_name(cond->traverse()));
    if (strcmp(cond_type, "int")) {
        semantic_error("A condicao deve ser uma expr do tipo int");
        exit(1);
    }
    auto expr_type = translate_type(type_name(expr->traverse()));
    if (strcmp(expr_type, "0")) {
        //fprintf(stderr, "O corpo de um while nao pode produzir valor\n");
        semantic_error("O corpo de um while nao pode produzir valor");
        exit(1);
    }
    nested_loops.pop();
    return g_tyenv.get("0");
}

void AstForNode::print(int indent_level) const
{   indent();
    printf("For %s:\n", this->id);
    indentp(2);
    puts("Lower:");
    this->lower->print(indent_level + 4);
    indentp(2);
    puts("Upper:");
    this->upper->print(indent_level + 4);
    indentp(2);
    puts("Expr:");
    this->expr->print(indent_level + 4);
}

Entry *AstForNode::traverse()
{
    g_valenv.enter();
    nested_loops.push(true);
    g_valenv.add(this->id, new ConstEntry(this->id, "int"), 0);
    auto lower_type = translate_type(type_name(lower->traverse()));
    auto upper_type = translate_type(type_name(upper->traverse()));
    if (strcmp(lower_type, "int") || strcmp(upper_type, "int")) {
        semantic_error("As expr de limites devem ser do tipo int");
        exit(1);
    }
    auto expr_type = translate_type(type_name(expr->traverse()));
    if (strcmp(expr_type, "0")) {
        semantic_error("O corpo de um for nao pode produzir valor\n");
        exit(1);
    }
    nested_loops.pop();
    g_valenv.leave();
    return g_tyenv.get("0");
}
