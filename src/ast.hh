#pragma once

#include "parser.hh"
#include "environments.hh"

//#include "llvm.h"
// Forward declaration.
namespace llvm {
    struct Value;
};

/*********************************************************************
 * AST
 *********************************************************************/
struct AstNode {
    virtual void print(int indent_level) const = 0;
    virtual Entry *traverse() = 0;
    virtual llvm::Value *codegen() const = 0;
};

// Used for the root ast node.
struct Ast {
    Ast(AstNode *root) : root(root) {}
    void print() { this->root->print(0); }
    void traverse() { this->root->traverse(); }
    void codegen(const char *out_file_name);
    AstNode *root;
};

/* BASE NODES
****************/
struct AstDecNode : public AstNode {
    virtual Entry *traverse_head() const = 0;
    virtual void codegen_head() const = 0;
};
struct AstLvalNode : public AstNode {};
struct AstExprNode : public AstNode {};


/* LISTS OF NODES
****************/
struct AstDecList {
    AstDecList(YYLTYPE loc, AstDecNode *dec, AstDecList *next) : loc(loc), dec(dec), next(next) {}
    void print(int indent_level) const;
    AstDecNode *dec;
    AstDecList *next;
    YYLTYPE loc;
};

struct AstExprList {
    AstExprList(YYLTYPE loc, AstExprNode *expr, AstExprList *next) : loc(loc), expr(expr), next(next) {}
    void print(int indent_level) const;
    AstExprNode *expr;
    AstExprList *next;
    YYLTYPE loc;
};

struct AstRecordArgsList {
    AstRecordArgsList(YYLTYPE loc, char *id, AstExprNode *expr, AstRecordArgsList *next)
        : loc(loc), id(id), expr(expr), next(next) {}
    void print(int indent_level) const;
    const char *id;
    AstExprNode *expr;
    AstRecordArgsList *next;
    YYLTYPE loc;
};

enum class Contract {
    in, out
};

struct AstContractList {
    AstContractList(YYLTYPE loc, Contract kind, AstExprNode *expr, AstContractList *next, char *id)
        : loc(loc), kind(kind), id(id), expr(expr), next(next) {}
    void print(int indent_level) const;
    const Contract kind;
    const char *id;
    AstExprNode *expr;
    AstContractList *next;
    YYLTYPE loc;
};

/*********************************************************************
 * DECLARATION NODES
 *********************************************************************/

/* TYPE DECLARATION NODES
*****************************************************************************************/
struct AstTyNode {
    virtual void print(int indent_level) const = 0;
};

struct AstTydecNode : public AstDecNode {
    AstTydecNode(YYLTYPE loc, char *name, AstTyNode *ty) : loc(loc), name(name), ty(ty) {}
    void print(int indent_level) const;
    Entry *traverse();
    Entry *traverse_head() const;
    void codegen_head() const;
    llvm::Value *codegen() const;
private:
    const char *name;
    AstTyNode *ty;
    YYLTYPE loc;
};

struct AstTyAliasNode : public AstTyNode {
    AstTyAliasNode(YYLTYPE loc, char *name) : loc(loc), name(name) {}
    void print(int indent_level) const;
    const char *name;
    YYLTYPE loc;
};

struct AstTyRecordFieldNode : public AstTyNode {
    AstTyRecordFieldNode(YYLTYPE loc, char *id, char *type, AstTyRecordFieldNode *next)
        : loc(loc), id(id), type(type), next(next) {}
    void print(int indent_level) const;
    const char *id, *type;
    AstTyRecordFieldNode *next;
    YYLTYPE loc;
};

struct AstTyRecordNode : public AstTyNode {
    AstTyRecordNode(YYLTYPE loc, AstTyRecordFieldNode *field_list) : loc(loc), fields(field_list) {}
    void print(int indent_level) const;
    AstTyRecordFieldNode *fields;
    YYLTYPE loc;
};

struct AstTyArrayNode : public AstTyNode {
    AstTyArrayNode(YYLTYPE loc, char *name) : loc(loc), name(name) {}
    void print(int indent_level) const;
    const char *name;
    YYLTYPE loc;
};

/* VAR DECLARATION NODE
*****************************************************************************************/
struct AstVardecNode : public AstDecNode {
    AstVardecNode(YYLTYPE loc, char *name, char *type, AstExprNode *value)
        : loc(loc), name(name), type(type), value(value) {}
    void print(int indent_level) const;
    Entry *traverse();
    Entry *traverse_head() const;
    void codegen_head() const;
    llvm::Value *codegen() const;
private:
    const char *name, *type;
    AstExprNode *value;
    YYLTYPE loc;
};

/* FUNCTION DECLARATION NODES
*****************************************************************************************/
struct AstFundecNode : public AstDecNode {
    AstFundecNode(YYLTYPE loc, const char *name, const char *type, AstTyRecordFieldNode *params,
                  AstExprNode *expr, AstContractList *contracts)
        : loc(loc), name(name), type(type), params(params), expr(expr), contracts(contracts) {}
    void print(int indent_level) const;
    Entry *traverse();
    Entry *traverse_head() const;
    void codegen_head() const;
    llvm::Value *codegen() const;
private:
    const char *name, *type;
    AstTyRecordFieldNode *params;
    AstExprNode *expr;
    AstContractList *contracts;
    YYLTYPE loc;
};


/*********************************************************************
 * L-VALUES
 *********************************************************************/
struct AstVarNode : public AstLvalNode {
    AstVarNode(YYLTYPE loc, char *name) : loc(loc), name(name) {};
    void print(int indent_level) const;
    Entry *traverse();
    llvm::Value *codegen() const;
    const char *name;
    YYLTYPE loc;
};

struct AstRecordNode : public AstLvalNode {
    AstRecordNode(YYLTYPE loc, AstLvalNode *lval, char *field) : loc(loc), lval(lval), field(field) { };
    void print(int indent_level) const;
    Entry *traverse();
    llvm::Value *codegen() const;
    AstLvalNode *lval;
    const char *field;
    char *type;
    int idx;
    YYLTYPE loc;
};

struct AstArrayNode : public AstLvalNode {
    AstArrayNode(YYLTYPE loc, AstLvalNode *lval, AstExprNode *idx) : loc(loc), lval(lval), idx(idx) {};
    void print(int indent_level) const;
    Entry *traverse();
    llvm::Value *codegen() const;
    AstLvalNode *lval;
    AstExprNode *idx;
    YYLTYPE loc;
};


/*********************************************************************
 * EXPRESSION NODES
 *********************************************************************/

/* BUILT-IN TYPE NODES
*************************/
struct AstIntegerNode : public AstExprNode {
    AstIntegerNode(YYLTYPE loc, int num) : loc(loc), val(num) {}
    void print(int indent_level) const;
    Entry *traverse();
    llvm::Value *codegen() const;
private:
    const int val;
    YYLTYPE loc;
};

struct AstStringNode : public AstExprNode {
    AstStringNode(YYLTYPE loc, char *str) : loc(loc), val(str) {}
    void print(int indent_level) const;
    Entry *traverse();
    llvm::Value *codegen() const;
private:
    const char *val;
    YYLTYPE loc;
};

struct AstNilNode : public AstExprNode {
    AstNilNode(YYLTYPE loc) : loc(loc) {};
    void print(int indent_level) const;
    Entry *traverse();
    llvm::Value *codegen() const;
    YYLTYPE loc;
};

// ()
struct AstNovalNode : public AstExprNode {
    AstNovalNode(YYLTYPE loc) : loc(loc) {};
    void print(int indent_level) const {
        printf("%*sNo value\n", indent_level, "");
    }
    Entry *traverse();
    llvm::Value *codegen() const;
    YYLTYPE loc;
};

struct AstBreakNode : public AstExprNode {
    AstBreakNode(YYLTYPE loc) : loc(loc) {};
    void print(int indent_level) const;
    Entry *traverse();
    llvm::Value *codegen() const;
    YYLTYPE loc;
};

/* UNARY OPERATORS
*********************/
struct AstUminusNode : public AstExprNode {
    AstUminusNode(YYLTYPE loc, AstExprNode *child) : loc(loc), child(child) {}
    void print(int indent_level) const;
    Entry *traverse();
    llvm::Value *codegen() const;
    AstExprNode *child;
    YYLTYPE loc;
};

/* BINARY OPERATORS
**********************/
enum class BinOp {
    // Arithmetic:
    plus, minus, mul, div,
    // Comparison:
    eq, neq, lt, le, gt, ge,
    // Boolean operators:
    aand, oor
};

struct AstBinopNode : public AstExprNode {
    AstBinopNode(YYLTYPE loc, BinOp op, AstExprNode *left, AstExprNode *right)
        : loc(loc), op(op), left(left), right(right) {}
    void print(int indent_level) const;
    Entry *traverse();
    llvm::Value *codegen() const;
private:
    const BinOp op;
    AstExprNode *left, *right;
    YYLTYPE loc;
};

/* SPECIAL EXPR NODES
*************************/
// let {decs} in {exprs} end
struct AstLetNode : public AstExprNode {
    AstLetNode(YYLTYPE loc, AstDecList *decs, AstExprList *exprs) : loc(loc), decs(decs), exprs(exprs) {};
    void print(int indent_level) const;
    Entry *traverse();
    llvm::Value *codegen() const;
private:
    AstDecList *decs;
    AstExprList *exprs;
    YYLTYPE loc;
};

// id / lval.id / lval[expr]
struct AstLvalExprNode : public AstExprNode {
    AstLvalExprNode(YYLTYPE loc, AstLvalNode *lval) : loc(loc), lval(lval) {}
    void print(int indent_level) const;
    Entry *traverse();
    llvm::Value *codegen() const;
    llvm::Value *getref() const;
private:
    AstLvalNode *lval;
    YYLTYPE loc;
};

// lval := expr
struct AstAssignNode : public AstExprNode {
    AstAssignNode(YYLTYPE loc, AstLvalExprNode *lval, AstExprNode *expr)
        : loc(loc), lval(lval), expr(expr) {}
    void print(int indent_level) const;
    Entry *traverse();
    llvm::Value *codegen() const;
private:
    AstLvalExprNode *lval;
    AstExprNode *expr;
    YYLTYPE loc;
};

// lval(arg1, arg2, ...)
struct AstFuncallNode : public AstExprNode {
    AstFuncallNode(YYLTYPE loc, char *name, AstExprList *args)
        : loc(loc), name(name), args(args) {}
    void print(int indent_level) const;
    Entry *traverse();
    llvm::Value *codegen() const;
private:
    const char *name;
    AstExprList *args;
    YYLTYPE loc;
};

// (exp, exp [, exp, ...])
struct AstExprSeqNode : public AstExprNode {
    AstExprSeqNode(YYLTYPE loc, AstExprList *exprs) : loc(loc), exprs(exprs) {}
    void print(int indent_level) const;
    Entry *traverse();
    llvm::Value *codegen() const;
private:
    AstExprList *exprs;
    YYLTYPE loc;
};

struct AstRecordCreationNode : public AstExprNode {
    AstRecordCreationNode(YYLTYPE loc, char *type_name, AstRecordArgsList *args)
        : loc(loc), type_name(type_name), args(args) {}
    void print(int indent_level) const;
    Entry *traverse();
    llvm::Value *codegen() const;
private:
    const char *type_name;
    AstRecordArgsList *args;
    YYLTYPE loc;
};

struct AstArrayCreationNode : public AstExprNode {
    AstArrayCreationNode(YYLTYPE loc, char *name, AstExprNode *len_expr, AstExprNode *init_expr)
        : loc(loc), name(name), len_expr(len_expr), init_expr(init_expr) {}
    void print(int indent_level) const;
    Entry *traverse();
    llvm::Value *codegen() const;
private:
    const char *name;
    AstExprNode *len_expr, *init_expr;
    YYLTYPE loc;
};

struct AstIfNode : public AstExprNode {
    AstIfNode(YYLTYPE loc, AstExprNode *cond, AstExprNode *then, AstExprNode *elsee)
        : loc(loc), cond(cond), then(then), elsee(elsee) {}
    void print(int indent_level) const;
    Entry *traverse();
    llvm::Value *codegen() const;
private:
    AstExprNode *cond, *then, *elsee;
    YYLTYPE loc;
};

struct AstWhileNode : public AstExprNode {
    AstWhileNode(YYLTYPE loc, AstExprNode *cond, AstExprNode *expr) : loc(loc), cond(cond), expr(expr) {}
    void print(int indent_level) const;
    Entry *traverse();
    llvm::Value *codegen() const;
private:
    AstExprNode *cond, *expr;
    YYLTYPE loc;
};

struct AstForNode : public AstExprNode {
    AstForNode(YYLTYPE loc, char *id, AstExprNode *lower, AstExprNode *upper, AstExprNode *expr)
        : loc(loc), id(id), lower(lower), upper(upper), expr(expr) {}
    void print(int indent_level) const;
    Entry *traverse();
    llvm::Value *codegen() const;
private:
    const char *id;
    AstExprNode *lower, *upper, *expr;
    YYLTYPE loc;
};
