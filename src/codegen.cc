#include "llvm.hh"

#include <cstdio>
#include <cstdlib>
#include <vector>

#include "ast.hh"

using namespace std;
using namespace llvm;

static LLVMContext g_context;
static Module g_module("main", g_context);
static IRBuilder<> g_builder(g_context);

typedef pair<Type *, Value *> Var;
static map<string, Var> *g_vars = NULL;
static map<string, Function *> *g_funcs = NULL;
static map<string, Type *> *g_types = NULL;

static Type *g_current_record_type = nullptr;
static BasicBlock *g_bb_break_target = nullptr;

struct {
    Type *void_t = Type::getVoidTy(g_context);
    Type *int_t = Type::getInt64Ty(g_context);
    Type *bool_t = Type::getInt1Ty(g_context);
    Type *string_t = PointerType::getUnqual(Type::getInt8Ty(g_context));
} llvm_types;

// TODO: look for user defined types.
static Type *llvm_type(const char *type_name)
{
    auto it = g_types->find(type_name);
    return (it != g_types->end()) ?
        it->second :
        llvm_types.void_t;
}

static AllocaInst *create_alloca(Function *foo, const char *var_name, Type *type)
{
  IRBuilder<> tmp_b(&foo->getEntryBlock(), foo->getEntryBlock().begin());
  return tmp_b.CreateAlloca(type, 0, var_name);
}

static Function *declare_func(const char *name, Type *retty, vector<Type*> params)
{
    FunctionType *fty = FunctionType::get(retty, params, false);
    return Function::Create(fty, Function::ExternalLinkage, name, &g_module);
}

static void add_gvar(const char *name, Value *val)
{
    assert(g_vars);
    Var var{val->getType(), val};
    auto it = g_vars->find(name);
    if (it == g_vars->end()) g_vars->insert({name, var});
    else it->second = var;
}

void Ast::codegen(const char *ofname)
{
/*
    InitializeNativeTarget();
    InitializeNativeTargetAsmPrinter();
    InitializeNativeTargetAsmParser();
*/
    g_vars = new map<string, Var>();
    {// Base types.
        g_types = new map<string, Type *>();
        g_types->insert({"int", llvm_types.int_t});
        g_types->insert({"string", llvm_types.string_t});
    }

    {// Declare std functions.
        g_funcs = new map<string, Function *>();
        vector<Type *> params = {};
        {// print(s: string).
            params.push_back(llvm_types.string_t);
            auto foo = declare_func("print", llvm_types.void_t, params);
            g_funcs->insert({"print", foo});
        } params.clear();
        {// printd(i: int).
            params.push_back(llvm_types.int_t);
            auto foo = declare_func("printd", llvm_types.void_t, params);
            g_funcs->insert({"printd", foo});
        } params.clear();
        {// flush().
            auto foo = declare_func("flush", llvm_types.void_t, params);
            g_funcs->insert({"flush", foo});
        } params.clear();
        {// getchar(): string.
            auto foo = declare_func("getchr", llvm_types.string_t, params);
            g_funcs->insert({"getchar", foo});
        } params.clear();
        {// ord(s: string): int.
            params.push_back(llvm_types.string_t);
            auto foo = declare_func("ord", llvm_types.int_t, params);
            g_funcs->insert({"ord", foo});
        } params.clear();
        {// chr(i: int): string.
            params.push_back(llvm_types.int_t);
            auto foo = declare_func("chr", llvm_types.string_t, params);
            g_funcs->insert({"chr", foo});
        } params.clear();
        {// size(s: string): int.
            params.push_back(llvm_types.string_t);
            auto foo = declare_func("size", llvm_types.int_t, params);
            g_funcs->insert({"size", foo});
        } params.clear();
        {// substring(s: string, first: int, n: int): string.
            params.push_back(llvm_types.string_t);
            params.push_back(llvm_types.int_t);
            params.push_back(llvm_types.int_t);
            auto foo = declare_func("substring", llvm_types.string_t, params);
            g_funcs->insert({"substring", foo});
        } params.clear();
        {// concat(s1: string, s2: string): string.
            params.push_back(llvm_types.string_t);
            params.push_back(llvm_types.string_t);
            auto foo = declare_func("concat", llvm_types.string_t, params);
            g_funcs->insert({"concat", foo});
        } params.clear();
        {// not(i: int): int.
            params.push_back(llvm_types.int_t);
            auto foo = declare_func("not", llvm_types.int_t, params);
            g_funcs->insert({"not", foo});
        } params.clear();
        {// exit(i: int).
            params.push_back(llvm_types.int_t);
            auto foo = declare_func("exit", llvm_types.void_t, params);
            g_funcs->insert({"exit", foo});
        } params.clear();
        {// _assert(i: int).
            params.push_back(llvm_types.int_t);
            params.push_back(llvm_types.string_t);
            auto foo = declare_func("_assert", llvm_types.void_t, params);
            g_funcs->insert({"_assert", foo});
        } params.clear();
        {// _strcmp(a: string, b: string).
            params.push_back(llvm_types.string_t);
            params.push_back(llvm_types.string_t);
            auto foo = declare_func("_strcmp", llvm_types.int_t, params);
            g_funcs->insert({"_strcmp", foo});
        } params.clear();
    }

    {// Creates an artificial function to be the implicit tigermain procedure.
        FunctionType *fty = FunctionType::get(llvm_types.int_t, false);
        Function *foo = Function::Create(fty, Function::ExternalLinkage, "tigermain", &g_module);
        BasicBlock *bb = BasicBlock::Create(g_context, "entry", foo);
        g_builder.SetInsertPoint(bb);
        Value *val = this->root->codegen();
        Value *cast = nullptr;
        if (val->getType() == llvm_types.bool_t)
            cast = g_builder.Insert(CastInst::CreateIntegerCast(val, llvm_types.int_t, true, "mainretcast"));
        else if (val->getType() == llvm_types.int_t)
            cast = val;
        else if (val->getType() == llvm_types.void_t)
            cast = Constant::getNullValue(Type::getInt64Ty(g_context));
        else {
            cast = g_builder.Insert(new PtrToIntInst(val, llvm_types.int_t, "mainretcast"));
        }
        g_builder.CreateRet(cast);
        verifyFunction(*foo);
/*
        auto child_expr = dynamic_cast<AstExprNode *>(this->root);
        assert(child_expr);
        this->root = new AstFundecNode("tigermain", nullptr, nullptr, child_expr, nullptr);
        this->root->codegen();
*/
    }

    error_code ec;
    raw_fd_ostream out(ofname, ec);

    g_module.print(out, nullptr);
}


/* DECS
*****************************************************************************************/
void AstTydecNode::codegen_head() const
{
    auto it = g_types->find(this->name);
    if (it == g_types->end())
        g_types->insert({this->name, nullptr});
}

Value *AstTydecNode::codegen() const
{
    {// Alias declaration.
        auto alias = dynamic_cast<const AstTyAliasNode *>(this->ty);
        if (alias) {
            auto rhs_it = g_types->find(alias->name); assert(rhs_it != g_types->end());
            auto lhs_it = g_types->find(this->name);
            if (lhs_it == g_types->end()) g_types->insert({this->name, rhs_it->second});
            else lhs_it->second = rhs_it->second;
        }
    }
    {// Record declaration.
        auto record = dynamic_cast<const AstTyRecordNode *>(this->ty);
        if (record) {
            auto vec = new FieldsVec();
            vector<Type *> elements;
            for (auto field = record->fields; field; field = field->next) {
                auto it = g_types->find(field->type); assert(it != g_types->end());
                Type *eltype = it->second;
                elements.push_back(eltype);
            }
            StructType *record = StructType::create(g_context, elements, this->name);
            auto it = g_types->find(this->name);
            if (it == g_types->end()) g_types->insert({this->name, record});
            else it->second = record;
        }
    }
    {// Array declaration.
        auto array = dynamic_cast<const AstTyArrayNode *>(this->ty);
        if (array) {
            // Get array type as a pointer to array->name's type.
            auto it = g_types->find(array->name); assert(it != g_types->end());
            Type *arr_type = it->second;
            if (arr_type->isStructTy())
                arr_type = PointerType::getUnqual(it->second);
            // Insert/Update this->name (tydec) type.
            it = g_types->find(this->name);
            if (it != g_types->end()) it->second = arr_type;
            else g_types->insert({this->name, arr_type});
        }
    }
    return Constant::getNullValue(Type::getInt64Ty(g_context));
}

void AstVardecNode::codegen_head() const
{
}

Value *AstVardecNode::codegen() const
{
    assert(g_vars);
    Function *foo = g_builder.GetInsertBlock()->getParent();
    if (this->type) {
        auto it = g_types->find(this->type); assert(it != g_types->end());
        g_current_record_type = it->second;
    }
    Value *val = this->value->codegen(); assert(val);
    auto var = new GlobalVariable(
        g_module, val->getType(), false, GlobalValue::CommonLinkage,
        Constant::getNullValue(val->getType()), this->name);
    add_gvar(this->name, var);
    return g_builder.CreateStore(val, var);
}

void AstFundecNode::codegen_head() const
{
    assert(g_funcs);

    vector <Type *> params = {};
    {// Compute params.
        for (auto par = this->params; par != nullptr; par = par->next) {
            params.push_back(llvm_type(par->type));
        }
    }
    Type *type = this->type ? llvm_type(this->type) : llvm_types.void_t;
    auto foo = declare_func(this->name, type, params);
    {// Add function to g_funcs if not already declared.
        auto it = g_funcs->find(this->name);
        if (it == g_funcs->end()) g_funcs->insert({this->name, foo});
        else it->second = foo;
    }
    {// Set names for all arguments.
        auto par = this->params;
        for (auto &arg : foo->args()) {
            arg.setName(par->id);
            par = par->next;
        } assert(par == nullptr);
    }

}

Value *AstFundecNode::codegen() const
{
    BasicBlock *bb_prev = g_builder.GetInsertBlock();
    auto prev_vars = g_vars; assert(g_vars);
    auto foound = g_funcs->find(this->name); assert(foound != g_funcs->end());
    Function *foo = foound->second;
    Type *type = foo->getFunctionType()->getReturnType();

    {// Clear foo's previous basic blocks and add new entry block.
        foo->getBasicBlockList().clear();
        BasicBlock *bb = BasicBlock::Create(g_context, "entry", foo);
        g_builder.SetInsertPoint(bb);
    }
    {// Add params to g_vars and create allocas for them.
        g_vars = new map<string, Var>(*g_vars);
        for (auto &par : foo->args()) {
            AllocaInst *alloca = create_alloca(foo, par.getName().str().c_str(), par.getType());
            g_builder.CreateStore(&par, alloca);

            auto it = g_vars->find(par.getName());
            if (it == g_vars->end()) g_vars->insert({par.getName(), {alloca->getType(), alloca}});
            else it->second = Var(alloca->getType(), alloca);
        }
    }
    {// Codegen the in contracts.
        for (auto contract = this->contracts; contract; contract = contract->next) {
            if (contract->kind == Contract::in) {
                Function *ass = g_funcs->find("_assert")->second;
                Value *cond = contract->expr->codegen();
                if (cond->getType() == llvm_types.bool_t) {
                    cond = g_builder.Insert(
                        CastInst::CreateIntegerCast(cond, llvm_types.int_t, true, ""));
                }
                vector<Value *> args_v {cond};
                char str[255] = "";
                snprintf(str, 255, "precondicao nao satisfeita na funcao [%s]", foo->getName());
                Value *msg = g_builder.CreateGlobalStringPtr(str, "inerror");
                args_v.push_back(msg);
                g_builder.CreateCall(ass, args_v, "");
            }
        }
    }
    {// Codegen the body.
        Value *retval = this->expr->codegen();

        {// Codegen the out contracts.
            for (auto contract = this->contracts; contract; contract = contract->next) {
                if (contract->kind == Contract::out) {
                    Function *ass = g_funcs->find("_assert")->second;

                    auto prev_vars = g_vars;
                    g_vars = new map<string, Var>(*g_vars);
                    if (contract->id) {
                        Value *alloca = create_alloca(foo, "outalloca", type);
                        g_builder.CreateStore(retval, alloca);
                        g_vars->insert({contract->id, {type, alloca}});
                    }
                    Value *cond = contract->expr->codegen();
                    if (cond->getType() == llvm_types.bool_t) {
                        cond = g_builder.Insert(
                            CastInst::CreateIntegerCast(cond, llvm_types.int_t, true, ""));
                    }
                    vector<Value *> args_v {cond};
                    char str[255] = "";
                    snprintf(str, 255, "poscondicao nao satisfeita na funcao [%s]", foo->getName());
                    Value *msg = g_builder.CreateGlobalStringPtr(str, "outerror");
                    args_v.push_back(msg);
                    g_builder.CreateCall(ass, args_v, "");

                    delete g_vars;
                    g_vars = prev_vars;
                }
            }
        }

        if (type == llvm_types.void_t)
            g_builder.CreateRetVoid();
        else
            g_builder.CreateRet(retval);
        verifyFunction(*foo);
    }
    {// Restore previous g_vars table and builder's insert point.
        delete g_vars; g_vars = prev_vars;
        g_builder.SetInsertPoint(bb_prev);
    }

    return foo;
}

/* LVALS
*****************************************************************************************/
Value *AstVarNode::codegen() const
{
    auto it = g_vars->find(this->name); assert(it != g_vars->end());
    const string& var_name = it->first;
    Type *var_type = it->second.first;
    Value *var_loc = it->second.second;
    return var_loc;
}

Value *AstRecordNode::codegen() const
{
    Value *rec = this->lval->codegen(); assert(rec);
    rec = g_builder.CreateLoad(rec, "struct"); assert(rec);
    vector<Value *> eidx = {
        ConstantInt::get(g_context, APInt(32, 0)),
        ConstantInt::get(g_context, APInt(32, this->idx)),
    };
    auto type_it = g_types->find(this->type); assert(type_it != g_types->end());
    Type *type = type_it->second;
    Value *gep = g_builder.CreateGEP(type, rec, eidx, "geprecordfield");
    return gep;
}

Value *AstArrayNode::codegen() const
{
    Value *arr = this->lval->codegen(); assert(arr);
    Value *idx = this->idx->codegen(); assert(idx);
    arr = g_builder.CreateLoad(arr, "array"); assert(arr);
    Value *gep = g_builder.CreateGEP(arr, idx, "geparrayidx"); assert(gep);
    return gep;
}

/* SPECIAL EXPRESSIONS
*****************************************************************************************/
Value *AstIntegerNode::codegen() const
{
    return ConstantInt::get(g_context, APInt(64, this->val, true));
}

Value *AstStringNode::codegen() const
{
    return g_builder.CreateGlobalStringPtr(this->val, "str");
}

Value *AstNilNode::codegen() const
{
    assert(g_current_record_type);
    return ConstantPointerNull::get(PointerType::getUnqual(g_current_record_type));
}

Value *AstNovalNode::codegen() const
{
    return Constant::getNullValue(Type::getInt64Ty(g_context));
}

Value *AstBreakNode::codegen() const
{
    assert(g_bb_break_target);
    return g_builder.CreateBr(g_bb_break_target);
}

Value *AstUminusNode::codegen() const
{
    Value *expr = this->child->codegen();
    // TODO: find out how to use unary minus.
    Value *zero = ConstantInt::get(g_context, APInt(64, 0, true));

    return g_builder.CreateSub(zero, expr, "uminustmp");
}

Value *AstBinopNode::codegen() const
{
    Value *lhs = this->left->codegen();
    Value *rhs = this->right->codegen();
    Value *zero = ConstantInt::get(g_context, APInt(64, 0, true));

    // If comparing strings, lhs becomes _strcmp result, and rhs becomes zero.
    if (lhs->getType() == llvm_types.string_t) {
        assert(rhs->getType() == llvm_types.string_t);
        auto it = g_funcs->find("_strcmp"); assert(it != g_funcs->end());
        Function *strcmp_foo = it->second;
        vector<Value *> args_v;

        args_v.push_back(lhs);
        args_v.push_back(rhs);

        lhs = g_builder.CreateCall(strcmp_foo, args_v, "streq_cmp");
        rhs = zero;
    }

    assert(lhs && rhs);

    switch (this->op) {
    case BinOp::plus:
        return g_builder.CreateAdd(lhs, rhs, "addtmp");
    case BinOp::minus:
        return g_builder.CreateSub(lhs, rhs, "mintmp");
    case BinOp::mul:
        return g_builder.CreateMul(lhs, rhs, "multmp");
    case BinOp::div:
        return g_builder.CreateSDiv(lhs, rhs, "divtmp");
    case BinOp::eq:
        return g_builder.CreateICmpEQ(lhs, rhs, "eqtmp");
    case BinOp::neq:
        return g_builder.CreateICmpNE(lhs, rhs, "neqtmp");
    case BinOp::lt:
        return g_builder.CreateICmpSLT(lhs, rhs, "lttmp");
    case BinOp::le:
        return g_builder.CreateICmpSLE(lhs, rhs, "letmp");
    case BinOp::gt:
        return g_builder.CreateICmpSGT(lhs, rhs, "gttmp");
    case BinOp::ge:
        return g_builder.CreateICmpSGE(lhs, rhs, "getmp");
    case BinOp::aand:
        return g_builder.CreateAnd(lhs, rhs, "andtmp");
    case BinOp::oor:
        return g_builder.CreateOr(lhs, rhs, "ortmp");
    default:
        fprintf(stderr, "Not implemented yet!\n");
        exit(1);
    }
}

Value *AstLetNode::codegen() const
{
    auto prev_vars = g_vars;
    auto prev_funcs = g_funcs;
    auto prev_types = g_types;

    {// New scope.
        g_vars = new map<string, Var>(*g_vars);
        g_funcs = new map<string, Function *>(*g_funcs);
        g_types = new map<string, Type *>(*g_types);
    }
    {// First add declarations to g_type/g_funcs/g_vars.
        auto declst = this->decs;
        while (declst) {
            declst->dec->codegen_head();
            declst = declst->next;
        }
    }
    {// Now generate declarations code.
        auto declst = this->decs;
        while (declst) {
            declst->dec->codegen();
            declst = declst->next;
        }
    }
    Value *last_expr = nullptr;
    {// Finally, generate let exprs code.
        auto exprlst = this->exprs;
        while (exprlst) {
            last_expr = exprlst->expr->codegen();
            exprlst = exprlst->next;
        }
    }

    {// Delete current scope tables and revert to previous ones.
        delete g_vars;
        g_vars = prev_vars;
        delete g_funcs;
        g_funcs = prev_funcs;
        delete g_types;
        g_types = prev_types;
    }

    if (last_expr) {
        return last_expr;
    } else {
        return ConstantInt::get(g_context, APInt(64, 0, true));
    }
}

Value *AstLvalExprNode::codegen() const
{
    Value *lval = this->lval->codegen(); assert(lval);
    char val_name[255];
    snprintf(val_name, 255, "load_lval_%s", lval->getName());
    Value *load = g_builder.CreateLoad(lval, val_name); assert(load);
    return load;
}

Value *AstLvalExprNode::getref() const
{
    Value *lval = this->lval->codegen(); assert(lval);
    return lval;
}

Value *AstAssignNode::codegen() const
{
    Value *lval = this->lval->getref();
    if (false) {// TODO: assign nil to rec.
        g_current_record_type = lval->getType()->getPointerElementType()->getPointerElementType();
    }
    Value *val = this->expr->codegen();
    g_builder.CreateStore(val, lval);

    return Constant::getNullValue(Type::getInt64Ty(g_context));
}

Value *AstFuncallNode::codegen() const
{
    assert(g_funcs);
    auto it = g_funcs->find(this->name); assert(it != g_funcs->end());
    Function *foo = it->second;
    vector<Value *> args_v;

    for (auto e = this->args; e; e = e->next) {
        args_v.push_back(e->expr->codegen());
        assert(args_v.back());
    }

    if (foo->getReturnType() == llvm_types.void_t)
        return g_builder.CreateCall(foo, args_v, "");
    return g_builder.CreateCall(foo, args_v, "calltmp");
}

Value *AstExprSeqNode::codegen() const
{
    auto exprlst = this->exprs;
    Value *last_expr = nullptr;
    while (exprlst) {
        last_expr = exprlst->expr->codegen();
        exprlst = exprlst->next;
    }
    return last_expr;
}

Value *AstRecordCreationNode::codegen() const
{
    auto it = g_types->find(this->type_name); assert(it != g_types->end());
    auto type_size = ConstantExpr::getSizeOf(it->second);
    Instruction *malloc = CallInst::CreateMalloc(
        g_builder.GetInsertBlock(), llvm_types.int_t, it->second, type_size,
        nullptr, nullptr, "malloctmp");
    int idx = 0;
    for (auto arg = this->args; arg; arg = arg->next, ++idx) {
        vector<Value *> eidx = {
            ConstantInt::get(g_context, APInt(32, 0)),
            ConstantInt::get(g_context, APInt(32, idx)),
        };
        Value *gep = g_builder.CreateGEP(it->second, malloc, eidx, "geprecordcreation");
        Value *val = arg->expr->codegen(); assert(val);
        g_builder.CreateStore(val, gep);
    }
    return g_builder.Insert(malloc);
}

Value *AstArrayCreationNode::codegen() const
{
    auto it = g_types->find(this->name); assert(it != g_types->end());
    Type *arr_type = it->second;
    Value *type_size = ConstantExpr::getSizeOf(arr_type);
    Value *arr_len = this->len_expr->codegen(); assert(arr_len);

    // Allows to assign nil to record array.
    //printf("Declarando Array de [%s]\n", arr_type->getName());
    if (arr_type->isPointerTy()) {
        g_current_record_type = arr_type->getPointerElementType();
    }

    Value *arr_val = this->init_expr->codegen(); assert(arr_val);

    Instruction *malloc = CallInst::CreateMalloc(
        g_builder.GetInsertBlock(), llvm_types.int_t, arr_type, type_size,
        arr_len, nullptr, "malloctmp");
    Value *result = g_builder.Insert(malloc);
    {// Initialize each value of the array with a for loop.
        // Initialize for.
        Function *foo = g_builder.GetInsertBlock()->getParent();
        AllocaInst *idx_var = create_alloca(foo, "array_forinit_var", llvm_types.int_t);
        Value *one = ConstantInt::get(g_context, APInt(64, 1, true));
        Value *zero = ConstantInt::get(g_context, APInt(64, 0, true));
        Value *vlower = zero;
        Value *vupper = arr_len;
        g_builder.CreateStore(vlower, idx_var);
        BasicBlock *bb_loop = BasicBlock::Create(g_context, "arrinit", foo);
        BasicBlock *bb_after = BasicBlock::Create(g_context, "arrinited");
        Value *vcond = g_builder.CreateICmpSLT(vlower, vupper, "precond");
        g_builder.CreateCondBr(vcond, bb_loop, bb_after);

        // Set array value at current idx.
        g_builder.SetInsertPoint(bb_loop);
        Value *gep_idx = g_builder.CreateLoad(llvm_types.int_t, idx_var, "gepidx");
        Value *gep = g_builder.CreateGEP(result, gep_idx, "arrgep");
        g_builder.CreateStore(arr_val, gep);

        // Step.
        Value *cur_var =
            g_builder.CreateLoad(llvm_types.int_t, idx_var, "cur_idx");
        Value *next_var = g_builder.CreateAdd(cur_var, one, "next_idx");
        g_builder.CreateStore(next_var, idx_var);

        // Loop back.
        vcond = g_builder.CreateICmpSLT(next_var, vupper, "arrcond");
        g_builder.CreateCondBr(vcond, bb_loop, bb_after);

        // After.
        foo->getBasicBlockList().push_back(bb_after);
        g_builder.SetInsertPoint(bb_after);
    }
    return result;
}

Value *AstIfNode::codegen() const
{
    Value *vcond = nullptr;
    {// Emit condition.
        vcond = this->cond->codegen(); assert(vcond);
        if (vcond->getType() == llvm_types.int_t) {
            Value *zero = ConstantInt::get(g_context, APInt(64, 0, true));
            vcond = g_builder.CreateICmpNE(vcond, zero, "ifcond");
        }
    }
    Function *foo = g_builder.GetInsertBlock()->getParent();
    BasicBlock *bb_then = BasicBlock::Create(g_context, "then", foo);
    BasicBlock *bb_else = BasicBlock::Create(g_context, "else");
    BasicBlock *bb_cont = BasicBlock::Create(g_context, "cont");
    Value *vthen = nullptr, *velse = nullptr;
    Value *result = nullptr;

    g_builder.CreateCondBr(vcond, bb_then, bb_else);
    {// Emit then block.
        g_builder.SetInsertPoint(bb_then);
        vthen = this->then->codegen(); assert(vthen);
        if (vthen->getType() != llvm_types.void_t &&
            vthen != Constant::getNullValue(Type::getInt64Ty(g_context))) {
            result = create_alloca(foo, "ifresult", vthen->getType());
            g_builder.CreateStore(vthen, result);
        }
        g_builder.CreateBr(bb_cont);
        bb_then = g_builder.GetInsertBlock();
    }
    {// Emit else block.
        foo->getBasicBlockList().push_back(bb_else);
        g_builder.SetInsertPoint(bb_else);
        if (this->elsee) {
            velse = this->elsee->codegen(); assert(velse);
            if (velse->getType() != llvm_types.void_t &&
                velse != Constant::getNullValue(Type::getInt64Ty(g_context))) {
                g_builder.CreateStore(velse, result);
            }
        }
        g_builder.CreateBr(bb_cont);
        bb_else = g_builder.GetInsertBlock();
    }
    {// Emit cont block;
        foo->getBasicBlockList().push_back(bb_cont);
        g_builder.SetInsertPoint(bb_cont);

        if (result && result != Constant::getNullValue(Type::getInt64Ty(g_context)))
            return g_builder.CreateLoad(result, "ifloadedresult");
        else
            return Constant::getNullValue(Type::getInt64Ty(g_context));
    }
    assert(false);
}

// TODO: Break?
Value *AstWhileNode::codegen() const
{
    Function *foo = g_builder.GetInsertBlock()->getParent();
    BasicBlock *bb_loop = BasicBlock::Create(g_context, "whilestart", foo);
    BasicBlock *bb_body = BasicBlock::Create(g_context, "whilebody");
    BasicBlock *bb_after = BasicBlock::Create(g_context, "whileafter");

    auto prev_break_target = g_bb_break_target;
    g_bb_break_target = bb_after;

    g_builder.CreateBr(bb_loop);
    // Condition.
    g_builder.SetInsertPoint(bb_loop);
    Value *vcond = this->cond->codegen(); assert(vcond);
    if (vcond->getType() == llvm_types.int_t) {
        Value *zero = ConstantInt::get(g_context, APInt(64, 0, true));
        vcond = g_builder.CreateICmpNE(vcond, zero, "loopcond");
    }
    g_builder.CreateCondBr(vcond, bb_body, bb_after);

    // Body.
    foo->getBasicBlockList().push_back(bb_body);
    g_builder.SetInsertPoint(bb_body);
    this->expr->codegen();
    g_builder.CreateBr(bb_loop);

    // After.
    foo->getBasicBlockList().push_back(bb_after);
    g_builder.SetInsertPoint(bb_after);

    g_bb_break_target = prev_break_target;

    return Constant::getNullValue(Type::getInt64Ty(g_context));
}

Value *AstForNode::codegen() const
{
    Function *foo = g_builder.GetInsertBlock()->getParent();
    Value *one = ConstantInt::get(g_context, APInt(64, 1, true));
    AllocaInst *idx_var = create_alloca(foo, this->id, llvm_types.int_t);

    Value *vlower = this->lower->codegen();
    Value *vupper = this->upper->codegen();

    g_builder.CreateStore(vlower, idx_var);

    BasicBlock *bb_loop = BasicBlock::Create(g_context, "forloop", foo);
    BasicBlock *bb_after = BasicBlock::Create(g_context, "forafter");

    auto prev_break_target = g_bb_break_target;
    g_bb_break_target = bb_after;

    // Evaluate if vlower < upper.
    Value *vcond = g_builder.CreateICmpSLE(vlower, vupper, "precond");
    g_builder.CreateCondBr(vcond, bb_loop, bb_after);

    {// Evaluate body.
        g_builder.SetInsertPoint(bb_loop);

        // Add idx_var to new g_vars.
        auto prev_vars = g_vars;
        g_vars = new map<string, Var>(*g_vars);
        g_vars->insert({this->id, {llvm_types.int_t, idx_var}});

        // Body.
        this->expr->codegen();

        // Restore previous g_vars.
        delete g_vars;
        g_vars = prev_vars;
    }

    // Step.
    Value *cur_var =
        g_builder.CreateLoad(idx_var->getAllocatedType(), idx_var, this->id);
    Value *next_var = g_builder.CreateAdd(cur_var, one, "next_idx");
    g_builder.CreateStore(next_var, idx_var);

    // Evaluate if var_idx < upper.
    vcond = g_builder.CreateICmpSLE(next_var, vupper, "cond");
    g_builder.CreateCondBr(vcond, bb_loop, bb_after);

    // After.
    foo->getBasicBlockList().push_back(bb_after);
    g_builder.SetInsertPoint(bb_after);
    g_bb_break_target = prev_break_target;

    return Constant::getNullValue(Type::getInt64Ty(g_context));
}

