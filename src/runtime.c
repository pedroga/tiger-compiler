#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>


void print(char *s)
{
    printf("%s", s);
}

void printd(int64_t i)
{
    char buf[255] = "";
    snprintf(buf, 255, "%ld", i);
    printf("%s", buf);
}

void flush(void)
{
    fflush(stdout);
}

// Allow to leak?
char *getchr(void)
{
    char *str = malloc(sizeof(*str) * 2);
    str[0] = getchar(); str[1] = '\0';
    return str;
}

int64_t ord(char *str)
{
    assert(str);
    return str[0] ? str[0] : -1;
}

// Allow to leak?
char *chr(int64_t i)
{
    assert(-255 < i && i < 255);
    char *str = malloc(sizeof(*str) * 2);
    str[0] = (char)i; str[1] = '\0';
    return str;
}

int64_t size(char *str)
{
    assert(str);
    int64_t s = 0;
    while (*str++)
        ++s;
    return s;
}


int _strcmp(char *s, char *t)
{
    return strcmp(s, t);
}


char *substring(char *str, int64_t first, int64_t n)
{
    assert(str);
    char *result = malloc(sizeof(*result) * (n + 1));
    int i;
    for (i = 0; (i < n) && (first + i < strlen(str)); ++i) {
        result[i] = str[first + i];
    } result[i] = '\0';
    return result;
}

char *concat(char *s1, char *s2)
{
    assert(s1 && s2);
    char *result = malloc(sizeof(*result) * (strlen(s1) + strlen(s2)));
    char *it = result;
    while ((*it++ = *s1++))
        ;
    while ((*it++ = *s2++))
        ;
    return result;
}

int64_t not(int64_t i)
{
    return !i;
}

void _assert(int64_t cond, char *msg)
{
    if (!cond) {
        puts(msg);
        exit(1);
    }
}

// Note: exit(i: int64_t) works already as stdlib's exit.

int main()
{
    extern int64_t tigermain(void);
    tigermain();
}
