#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include "ast.hh"
#include "environments.hh"
#include "parser.hh"

extern Ast *ast;

extern FILE *yyin;

int main(int argc, char **argv)
{
    char ofname[255] = "", irname[258] = "";
    bool print_tree = false, print_ir = false;
    int opt;
    if (argc < 2) {
        printf("Opções:\n  -p <prog.tig> // nome programa de entrada \n"
               "  -a //imprime a arvore sintatica abstrata\n");
        return 0;
    }
    while ((opt = getopt(argc, argv, "haip:o:")) != -1) {
	switch (opt) {
	case 'h':
	    printf("Opções:\n  -p <prog.tig> // nome programa de entrada \n"
                   "  -o <prog> // nome da saida\n"
                   "  -a // imprime a arvore sintatica abstrata\n"
                   "  -i // gera um arquivo contendo a representacao intermediaria\n");
	    return 0;
        case 'a':
            print_tree = true;
            break;
        case 'i':
            print_ir = true;
            break;
        case 'o': {
            strncpy(ofname, optarg, 255);
            strcpy(irname, ofname);
            strcat(irname, ".ll");
        } break;
	case 'p':
	    if (!(yyin = fopen(optarg, "r"))) {
		perror(optarg);
                exit(1);
	    }
	    break;
        }
    }
    yyparse();
    if (print_tree)
        ast->print();
    puts("**Syntax OK, starting semantic analisis...");
    /* TODO:
     * - verificar recursao direta;
     * - redefinicao de funcao no mesmo escopo (let) deve manter o prototipo.
     */
    ast->traverse();
    puts("**Semantics OK, starting intermediate code generation...");

    ast->codegen(irname);
    puts("**Codegen OK, compiling executable...");
    char cmd[255] = "";
    snprintf(cmd, 255, "clang-10 runtime.ll %s -o %s", irname, ofname);
    system(cmd);
    if (!print_ir) {
        snprintf(cmd, 255, "rm %s", irname);
        system(cmd);
    }
}
