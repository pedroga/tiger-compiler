#!/bin/bash

function test_project {
    local dir="../moodlep-testes/projeto$1/"
    if [[ ! -d $dir ]]; then
        echo "Diretorio invalido: $dir"
        return
    fi

    for file in $(find $dir -type f -name '*.tig'); do
        echo "**********************************************************************"
        echo "** COMPILANDO: $file"
        ../src/tc -p $file -o $(basename -s .tig -- $file) -i
        echo "**********************************************************************"
        echo
    done
}

test_project $1
